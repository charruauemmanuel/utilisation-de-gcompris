# La balance de Roberval

## Répartis les poids pour équilibrer la balance.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-grandeurs-et-mesures/scalesboard.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Calcul mental, égalité arithmétique.




!!! info "Règle du jeu"

    Pour équilibrer la balance, déplace des poids sur le plateau de gauche ou de droite. Les poids peuvent être placés dans n'importe quel ordre.




___
![](../images/scalesboard.png){ width=50%;  : .center }

___
