# Calendrier

## Sélectionne la date demandée sur le calendrier.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-grandeurs-et-mesures/calendar.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à utiliser un calendrier.




!!! info "Règle du jeu"

    Sélectionne la date demandée sur le calendrier et confirme ensuite ta réponse en appuyant sur le bouton « Ok ».<br><br>Pour certains niveaux, tu devras trouver le jour de la semaine pour une date donnée. Dans ce cas, clique sur le jour correspondant de la semaine dans la liste.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer parmi les réponses</li><li>Espace ou Entrée : valider ta réponse</li></ul>




___
![](../images/calendar.png){ width=50%;  : .center }

___
