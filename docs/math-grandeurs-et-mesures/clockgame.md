# Horloges

## Présentation générale de l'activité

### Apprends à lire l'heure sur une montre à aiguilles.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-grandeurs-et-mesures/clockgame.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Distinguer les différences entre les heures, minutes et secondes. Régler et afficher l'heure sur une montre à aiguilles.




!!! info "Règle du jeu"

    Régler l'horloge à la bonne heure. Ajuster les différentes aiguilles pour les placer au bon endroit. L'aiguille la plus courte indique les heures.




___
![](../images/clockgame.png){ width=50%;  : .center }

___
## Séances créées par des utilisateurs utilisant cette activitée

### Séance 1: Utiliser GCompris pour apprendre à lire l'heure

#### Présentation du fonctionnement d'une horloge sans GCompris
Montrer une vraie horloge, demander l'usage des trois aiguilles, les dessiner au tableau.
S'il n'y a pas de vraie horloge l'activité Horloge peut être utilisée.

"La plus petite aiguille est l'aiguille des heures, elle fait un tour d'horloge en 12 heures."

"L'aiguille des minutes est plus longues et fait le tour de l'horloge en 60 minutes ce qui correspond à 1 heure.

" L'aiguille la plus fine et le plus longue fait le tour de l'écran en 60 secondes ce qui correspond à 1 minute.

Reprendre le fonctionnement de l'hologe avec les élèves :
Faire tourner l'aiguille des minutes et demander pourquoi l'aiguille des heures se déplace tout doucement.
Revenir sur le nom et le rôle de chaque aiguille.

#### Utiliser GCompris
Pour présenter le fonctionnement de l'aiguille des heures, choisir l'option heure entière.
Ouvir la fenêtre de configuration



![](./images/horloge_menu_option.png){ width=60%;  : .center }



horloge_menu_option

![](./images/horloge_choisir_heure.png){ width=60%;  : .center }

Choisir l'option "Heures entières".

![](./images/horloge_option_heure_entière.png){ width=60%;  : .center }





Montrer aux élèves comment répondre à plusieurs questions, bien expliciter la démarche mentale puis faire participer les élèves qui doivent donner la réponse et expliquer ce que le professeur doit faire pour répondre.



Quelques élèves passent au tableau et expliquent ce qu'ils font pour répondre aux questions proposées.

Distribuer la fiche d'exerices suivante aux élèves et la faire glisser dans une fiche plastique pour qu'il s'entrainent sur un support papier.

[Fiche d'exercices](./images/horloge-exercices.pdf)


La gestion de GCompris au VPI peut être confiée à un élève afin de pouvoir circuler pour vérifier le travail des élèves.

Cette séance peut être adaptée à différents niveaux de classes, il faut alors changer le niveau de difficulté dans le menu de configuration.

![](./images/horloge_changer_difficulté.png){ width=60%;  : .center }