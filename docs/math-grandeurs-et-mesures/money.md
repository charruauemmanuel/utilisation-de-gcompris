# L'argent

## Manipulation des billets et pièces.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-grandeurs-et-mesures/money.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à compter et utiliser de la monnaie.




!!! info "Règle du jeu"

    Clique ou tape sur les pièces ou les billets en bas de l'écran pour payer. Si tu désires retirer une pièce, clique ou tape dessus dans la zone du haut.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches gauche et droite : se déplacer dans la zone</li><li>Espace ou Entrée : choisir un objet</li><li>Tab : naviguer entre la zone basse et la zone haute</li></ul>




___
![](../images/money.png){ width=50%;  : .center }

___
