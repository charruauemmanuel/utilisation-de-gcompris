# Équilibre en utilisant le Système international d'unités

## Attrape et fais glisser des poids pour équilibrer la balance et calculer la masse.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-grandeurs-et-mesures/scalesboard_weight.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Calcul mental, égalité arithmétique, conversion d'unité.




!!! info "Règle du jeu"

    Pour équilibrer la balance, déplace les masses sur le plateau de gauche. Les masses peuvent être placées dans n'importe quel ordre. Attention aux masses et à leurs unités, rappelle-toi qu'un kilogramme (kg) représente 1000 grammes (g).




___
![](../images/scalesboard_weight.png){ width=50%;  : .center }

___
