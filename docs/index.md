---
author: Emmanuel Charruau
title: 🏡 Accueil
---

# Guide participatif à l'utilisation de GCompris

![Bannière de GCompris](./images/gcompris_banner.png)


![GCompris](./images/gcompris_presentation.png){ width=50%;  : .center }



## GCompris

!!! info "GCompris"

    GCompris est un logiciel éducatif qui propose des activités variées aux enfants de 2 à 10 ans.

    Les activités sont quelquefois ludiques, mais toujours pédagogiques.

    Voici la liste des catégories d'activités avec quelques exemples :

    - découverte de l'ordinateur : clavier, souris, écrans tactiles…
    - lecture : lettres, mots, entraînement à la lecture, à l'écriture…
    - mathématiques : révision des tables, dénombrement, les tables à double entrée…
    - sciences : l'écluse, le cycle de l'eau, l'énergie renouvelable…
    - géographie : pays, régions, culture…
    - jeux : les échecs, jeux de mémoire, le puissance 4, le pendu, le morpion…
    - autres : couleurs, formes, le Braille, apprendre à dire l'heure…

    En tout, GCompris propose plus de 100 activités et il continue à évoluer. GCompris est un logiciel libre, il vous est donc possible de l'adapter à votre besoin ou de l'améliorer, et pourquoi pas, d'en faire bénéficier les enfants du monde entier.

    Le projet GCompris est hébergé et développé par la communauté KDE.

## Utiliser ce guide et participez à son enrichissement

Le menu de gauche permet d'accéder aux description des activités de GCompris. Celles-ci sont regroupées en domaines pour faciliter la recherche.

Ce guide est **participatif**. Vous pouvez retrouver toutes les activités (plus de 190 à ce jour) et ajouter à la page de l'activité recherchée votre séance expliquant comment vous l'utilisez pour atteindre votre objectif pédagogique.

En haut de chaque page d'activité vous trouverez deux bouton, un bouton accédant à un tutoriel vidéo qui explique comment rajouter vos séances, et un bouton pour rajouter la séance.

![GCompris](./images/boutons_participation.png){ width=50%;  : .center }

Vous pouvez consulter la page de [Horloges](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/math-grandeurs-et-mesures/clockgame/) afin d'avoir un exemple de contribution à ce guide.

---
Merci d’avance pour vos contributions précieuses ! 😊

