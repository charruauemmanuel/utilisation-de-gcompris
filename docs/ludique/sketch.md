# Sketch

## Use the various digital painting tools to draw images.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ludique/sketch.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Learn about some digital painting tools.




!!! info "Règle du jeu"

    Select tools and colors from the foldable panels and draw images.<br><br><b>Keyboard controls:<ul><li>Ctrl + Z: undo last action</li><li>Ctrl + Y: redo last action</li><li>Ctrl + S: save the image</li><li>Ctrl + O: open an image</li><li>Ctrl + N, Delete or Backspace: create a new image</li></ul>




___
![](../images/sketch.png){ width=50%;  : .center }

___
