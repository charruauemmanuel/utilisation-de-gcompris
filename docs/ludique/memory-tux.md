# Jeu de mémoire visuelle contre Tux

## Retourne les cartes et retrouve les paires d'images. Tu joues contre Tux.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ludique/memory-tux.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entrainer sa mémoire visuelle.




!!! info "Règle du jeu"

    Un ensemble de cartes est affiché, côté face caché. Chaque carte est présente en double exemplaire. En cliquant sur une carte, on découvre sa face cachée et il faut trouver les paires correspondantes. Cependant, on ne peut découvrir que deux faces cachées à la fois.<br/>Il faut se rappeler la position des images pour les associer deux à deux entre elles. Une paire de cartes identiques est enlevée lorsqu'elle est découverte.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace et Entrée : retourner la carte sélectionnée</li></ul>




___
![](../images/memory-tux.png){ width=50%;  : .center }

___
