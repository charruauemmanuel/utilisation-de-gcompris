# Dessin dans une grille

## Crée ton propre dessin.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ludique/simplepaint.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Exprimez votre créativité par le dessin.




!!! info "Règle du jeu"

    Choisis une couleur et colore les cases comme tu le souhaites pour créer un dessin.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace ou Entrée : peindre</li><li>Tab : bascule entre le sélectionneur de couleur et la zone de peinture.</li></ul>




___
![](../images/simplepaint.png){ width=50%;  : .center }

___
