# Jeu d'équilibre

## Fais rouler la balle jusqu'à la porte en inclinant le plateau.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ludique/balancebox.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entraînement à la motricité et au comptage.




!!! info "Règle du jeu"

    Guide la bille vers la porte. Fais attention à ne pas la faire tomber dans un trou. La porte s'ouvrira si tu touches les nombres proposés dans le bon ordre. Pour déplacer la bille, fais pencher ton téléphone ou ta tablette. Sur PC utilise les flèches du clavier.<br><br>Dans le menu de configuration de l'activité, tu peux choisir entre l'option par défaut « Intégré » et l'option « Utilisateur » qui te permettra de définir tes propres niveaux. Pour les créer, sélectionne la collection de niveaux et démarre l'éditeur de niveaux en cliquant sur le bouton correspondant.<br><br>Avec <b>l'éditeur de niveaux</b> tu peux créer tes propres niveaux. Choisis un des outils d'édition localisés sur le bord pour modifier la carte contenue par le niveau actif de l'éditeur :<ul><li>Croix : effacer le contenu d'une case</li><li>Mur horizontal : ajouter/supprimer un mur horizontal sur la partie basse de la case</li><li>Mur vertical : ajouter/supprimer un mur vertical sur la partie droite de la case</li><li>Trou : ajouter/supprimer un trou dans une case</li><li>Bille : placer la position initiale de la bille</li><li>Porte : placer la position de la porte</li><li>Contact : ajouter/supprimer un bouton de contact. Tu peux ajuster la valeur du bouton de contact en utilisant le compteur. Il n'est pas possible de fixer une même valeur plus d'une fois dans une carte.</li></ul>Tous les outils (excepté l'outil de suppression) sélectionnent l'élément qui leur est rattaché à partir de la cellule cliquée : un élément peut être placé en cliquant sur une cellule vide. En cliquant de nouveau sur la même cellule avec le même outil, tu peux le supprimer.<br><br>Tu peux tester un niveau modifié en cliquant sur le bouton « Essayer » situé à côté de la vue d'édition. Tu peux quitter le mode de test en cliquant sur le bouton d'accueil situé dans la barre de commande ou en pressant la touche d'échappement sur le clavier ou le bouton de retour de ton appareil mobile.<br><br>Pour changer le niveau actuellement édité, utilise les boutons représentant des flèches présents sur la barre de l'éditeur. De retour dans l'éditeur tu peux continuer à éditer le niveau actuel et le tester de nouveau si c'est nécessaire.Lorsque ton niveau est terminé tu peux l'enregistrer dans un fichier en cliquant sur le bouton « Enregistrer » présent sur le côté.<br><br>Pour retourner à l'écran de configuration des activités, clique sur le bouton d'accueil de la barre, ou appuie sur la touche d'échappement sur ton clavier ou la touche retour sur ton appareil mobile.<br><br>Pour terminer, pour charger ta collection de niveaux, clique sur le bouton « Charger les niveaux existants ».




___
![](../images/balancebox.png){ width=50%;  : .center }

___
