# Trouve les différences

## Trouve les différences entre les deux images.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ludique/photo_hunter.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer sa perception des détails et sa concentration visuelle.




!!! info "Règle du jeu"

    Observe les deux images attentivement. Il y a quelques petites différences. Quand tu trouves une différence, tu dois cliquer dessus.




___
![](../images/photo_hunter.png){ width=50%;  : .center }

___
