# Labyrinthe invisible

## Guide Tux hors du labyrinthe invisible.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ludique/mazeinvisible.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à se repérer dans l'espace, à mémoriser une position et un parcours.




!!! info "Règle du jeu"

    Utilise les flèches pour déplacer Tux vers la porte. Utilise l'icône « labyrinthe » ou la barre d'espace pour basculer entre les modes invisible et visible. Le mode visible te donne juste une indication sur ta position, comme une carte. Tu ne peux pas déplacer Tux en mode visible.<br><br>Dans les premiers niveaux, Tux marche paisiblement dans le labyrinthe, un pas à chaque fois.<br><br>Pour les labyrinthes plus grands, il y a un mode de déplacement spécial, appelé « mode-course-rapide ». Lorsque ce mode est activé, Tux va courir automatiquement, jusqu'à ce qu'il rencontre une bifurcation. Tu dois alors décider dans quelle direction aller.<br><br>Tu peux savoir si tu es dans ce mode en regardant les pieds de Tux : s'il est pieds nus, le mode « mode-course-rapide » est désactivé. S'il porte des chaussures de sport rouges, ce mode est activé.<br><br>Pour les niveaux avancés, le « mode-course-rapide » est activé automatiquement. Si tu veux l'utiliser pour les premiers niveaux ou si tu veux le désactiver pour les niveaux élevés, clique sur l'icône « pieds nus / chaussures de sport » dans le coin en haut à gauche de l'écran pour le (dés)activer.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace : bascule entre les modes invisible et visible.</li></ul>




___
![](../images/mazeinvisible.png){ width=50%;  : .center }

___
