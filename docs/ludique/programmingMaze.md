# Programmer le chemin

## Programme le chemin de Tux pour qu'il attrape le poisson.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ludique/programmingMaze.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à programmer des instructions pour se déplacer en suivant un chemin.




!!! info "Règle du jeu"

    Choisis les instructions à partir du menu. Mets les dans l'ordre afin d'aider Tux à atteindre son objectif.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches gauche et droite : se déplacer dans la zone sélectionnée</li><li>Flèches du haut et du bas : augmente ou réduit la temporisation de la boucle si la zone de boucle est sélectionnée.</li><li>Espace : sélectionne une instruction ou ajoute l'instruction sélectionnée dans la zone principale / de procédure / boucle.</li><li>Tabulation : bascule entre la zone d'instructions et la zone principale / de procédure / de boucle.</li><li>Supprimer : enlève l'instruction sélectionnée de la zone principale / de procédure / de boucle.</li><li>Entrée : exécute le code ou réinitialise Tux a échoué à atteindre le poisson</li></ul><br>Pour ajouter une instruction dans la zone principale / de procédure / de boucle, sélectionne la à partir de la zone d'instructions, bascule ensuite vers la zone principale / de procédure / de boucle puis appuies sur « Espace ».<br><br>Pour modifier une instruction dans la zone principale / de procédure / de boucle, sélectionne la dans cette zone, ensuite, bascule vers la zone d'instructions, choisis la nouvelle instruction et enfin appuies sur « Espace ».




___
![](../images/programmingMaze.png){ width=50%;  : .center }

___
