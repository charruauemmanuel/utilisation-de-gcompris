# Hexagone

## Localise la fraise en cliquant sur les hexagones bleus.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ludique/hexagon.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Activité d'entraînement à la stratégie.




!!! info "Règle du jeu"

    Trouve la fraise dans le champ d'hexagones bleus. Les champs deviennent de plus en plus rouges au fur et à mesure que tu te rapproches de sa position.




___
![](../images/hexagon.png){ width=50%;  : .center }

___
