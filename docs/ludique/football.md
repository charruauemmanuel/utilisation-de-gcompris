# Jeu de football

## Envoie le ballon dans les buts.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ludique/football.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Comprendre les notions de direction et de vitesse.




!!! info "Règle du jeu"

    Pour tirer, appuie sur la balle puis tire vers l'arrière pour créer une ligne de visée. Plus la ligne est longue plus le tir sera fort. Le ballon suivra la direction de la ligne au moment du lâcher. Le but est validé lorsque le ballon atteint l'autre bord du terrain.




___
![](../images/football.png){ width=50%;  : .center }

___
