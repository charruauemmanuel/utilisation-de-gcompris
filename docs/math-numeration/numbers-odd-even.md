# Nombres pairs et impairs

## Déplace l'hélicoptère pour attraper les nuages contenant des nombres pairs ou impairs.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/numbers-odd-even.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à reconnaître les nombres pairs et impairs et leur ordre numérique.




!!! info "Règle du jeu"

    Attrape les nuages avec des nombres pairs ou impairs, dans le bon ordre. Avec le clavier, utilise les touches flèches pour déplacer l'hélicoptère. Avec un périphérique de pointage, clique ou tape simplement sur l'endroit ou tu veux aller. Pour savoir quel nombre tu dois attraper, tu peux soit t'en souvenir soit vérifier dans le coin en bas à droite.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : déplacer l'hélicoptère.</li></ul>




___
![](../images/numbers-odd-even.png){ width=50%;  : .center }

___
