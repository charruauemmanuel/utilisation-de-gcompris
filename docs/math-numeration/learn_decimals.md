# Apprends les nombres décimaux

## Représente le nombre décimal donné.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/learn_decimals.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Associer l'écriture décimale d'un nombre à sa représentation graphique.




!!! info "Règle du jeu"

    Un nombre décimal est affiché. Fais glisser la flèche pour sélectionner une partie de la barre, puis fais glisser la partie sélectionnée de la barre vers la zone vide. Répète ces étapes jusqu'à ce que le nombre de barres déposées corresponde au nombre décimal affiché. Clique ensuite sur le bouton « Ok » pour valider ta réponse.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Entrée : pour valider tes réponses</li></ul>




___
![](../images/learn_decimals.png){ width=50%;  : .center }

___
