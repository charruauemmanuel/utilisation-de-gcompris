# Compte les éléments

## Déplace les éléments et arrange-les au mieux pour pouvoir les compter.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/enumerate.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    S'entrainer à dénombrer des petites quantités.




!!! info "Règle du jeu"

    Commence par organiser correctement les éléments afin de pouvoir les compter. Ensuite, sélectionne l'élément pour lequel tu désires répondre dans la zone de gauche. Saisis la réponse avec le clavier.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèche du haut : choisir le prochain objet</li><li>Flèche du bas : choisir l'objet précédent</li><li>Clavier numérique : saisir ta réponse correspondant à l'élément sélectionné</li><li>Entrée : valider ta réponse (si l'option « Valide les réponses » est assignée au bouton « Ok »</li></ul>




___
![](../images/enumerate.png){ width=50%;  : .center }

___
