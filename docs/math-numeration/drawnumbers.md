# Dessine les nombres

## Connecter les points pour dessiner les chiffres de 0 à 9.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/drawnumbers.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à dessiner les nombres d'une façon ludique.




!!! info "Règle du jeu"

    Dessine les nombres en connectant les points dans le bon ordre.




___
![](../images/drawnumbers.png){ width=50%;  : .center }

___
