# Ordonne les nombres

## Ordonne les nombres donnés selon un ordre croissant ou décroissant comme demandé.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/ordering_numbers.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Savoir lire et ordonner des nombres.




!!! info "Règle du jeu"

    Fais les glisser et dépose les nombres dans la zone supérieure dans un ordre croissant ou décroissant, comme demandé.




___
![](../images/ordering_numbers.png){ width=50%;  : .center }

___
