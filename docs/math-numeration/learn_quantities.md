# Crée une collection

## Apprendre à représenter une quantité d'objets.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/learn_quantities.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à créer une collection, compte combien d'oranges sont nécessaires pour représenter la quantité demandée.




!!! info "Règle du jeu"

    Une quantité est demandée. Fais glisser la flèche pour sélectionner un nombre d'oranges, puis fais glisser les oranges sélectionnées vers la zone vide. Répète ces étapes jusqu'à ce que le nombre d'oranges déposées corresponde à la quantité demandée. Clique sur le bouton « Ok » pour valider ta réponse.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Entrée : pour valider tes réponses</li></ul>




___
![](../images/learn_quantities.png){ width=50%;  : .center }

___
