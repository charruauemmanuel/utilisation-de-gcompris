# Compte les intervalles

## Tux a faim. Aide-le à trouver des poissons en comptant les glaçons nécessaires pour les atteindre.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/reversecount.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à compter des intervalles.




!!! info "Règle du jeu"

    D'abord, compte de combien de glaçons doit se déplacer Tux pour atteindre le poisson. Ensuite, clique sur le domino pour sélectionner le bon nombre. Tu peux aussi utiliser le bouton droit de la souris pour compter à l'envers. Quand tu as terminé, clique sur le bouton « Ok » ou utilise la touche « Entrée ».




___
![](../images/reversecount.png){ width=50%;  : .center }

___
