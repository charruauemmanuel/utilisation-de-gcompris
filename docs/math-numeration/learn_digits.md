# Apprends les chiffres

## Représente la quantité correspondant au nombre composé d'un seul chiffre donné.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/learn_digits.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre les quantités correspondants aux nombres composés d'un seul chiffre.




!!! info "Règle du jeu"

    Un chiffre est affiché à l'écran. Clique sur les cercles pour obtenir la quantité de cercles remplis correspondant au nombre représenté par ce chiffre.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace : sélectionner ou désélectionner un cercle.</li><li>Entrée : pour valider tes réponses</li><li>Tab : répéter le nombre</li></ul>




___
![](../images/learn_digits.png){ width=50%;  : .center }

___
