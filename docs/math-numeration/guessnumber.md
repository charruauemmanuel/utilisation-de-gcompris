# Trouve le nombre cible

## Aide Tux à sortir de la grotte. Tux cache un nombre que tu dois découvrir.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/guessnumber.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    S'entrainer à comparer des nombres.




!!! info "Règle du jeu"

    Lis les instructions qui donnent l'intervalle dans lequel se trouve la valeur à deviner. Inscris un nombre dans la zone située en haut à droite. On te dira si ton nombre est trop grand ou trop petit. Tu peux alors choisir un autre nombre. La distance qui sépare Tux et la zone de sortie sur la droite représente la quantité qui te sépare de la bonne valeur. Selon que Tux est au-dessus ou au-dessous du niveau de la sortie, ton nombre est supérieur ou inférieur à la bonne valeur.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Clavier numérique : saisir un nombre</li><li>Retour arrière : supprimer un nombre</li></ul>




___
![](../images/guessnumber.png){ width=50%;  : .center }

___
