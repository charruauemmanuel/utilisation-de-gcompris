# Compare les nombres

## Regarde les nombres et choisis le bon signe de comparaison.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/comparator.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à comparer les nombres.




!!! info "Règle du jeu"

    Choisis une paire de nombres dans la liste. Ensuite, choisis le bon opérateur de comparaison pour cette paire. Quand tu auras répondu pour chaque ligne, appuie sur le bouton Ok pour valider tes réponses.<br><br>Si des réponses sont incorrectes, une croix apparaîtra sur les lignes correspondantes. Corrige ces erreurs et appuie sur le bouton Ok pour valider tes nouvelles réponses.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèche du haut et du bas : naviguer dans la liste</li><li>Flèches gauche et droite : naviguer dans les boutons de signes</li><li>Espace : si un signe est sélectionné, saisir sur ce signe</li><li>Entrée : valider tes réponses</li><li>&lt;, &gt; ou = : saisir le signe correspondant</li></ul>




___
![](../images/comparator.png){ width=50%;  : .center }

___
