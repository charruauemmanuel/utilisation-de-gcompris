# Repérage sur ligne graduée

## Lis les valeurs sur la ligne graduée.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/graduated_line_read.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à te repérer sur une ligne graduée.




!!! info "Règle du jeu"

    Saisis sur le clavier numérique ou sur ton clavier le nombre correspondant à la valeur recherchée sur la ligne.<br><b>Pour contrôler avec le clavier :</b><ul><li>Clavier numérique : saisir un nombre</li><li>Touche retour arrière : effacer le dernier chiffre</li><li>Supprimer : réinitialiser ta réponse</li><li>Espace ou Entrée : valider ta réponse</li></ul>




___
![](../images/graduated_line_read.png){ width=50%;  : .center }

___
