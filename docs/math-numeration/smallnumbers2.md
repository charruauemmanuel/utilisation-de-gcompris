# Les nombres avec des dominos

## Dénombre les quantités sur les dominos et donne leur résultat avant qu'ils ne touchent le sol.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/smallnumbers2.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à dénombrer dans un temps limité.




!!! info "Règle du jeu"

    Avec le clavier, saisis le nombre correspondant au nombre de points indiqués par le dé qui tombe.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Clavier numérique : saisir ta réponse</li></ul>




___
![](../images/smallnumbers2.png){ width=50%;  : .center }

___
