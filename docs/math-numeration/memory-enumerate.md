# Jeu de mémoire sur le comptage

## Retourne les cartes pour associer un nombre et une image dessinée.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/memory-enumerate.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entraînement à la numération en faisant jouer la mémoire.




!!! info "Règle du jeu"

    Chaque carte cache soit une image avec un nombre d'éléments, soit un nombre. Tu dois associer les nombres avec les images correspondantes.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace et Entrée : retourner la carte sélectionnée</li></ul>




___
![](../images/memory-enumerate.png){ width=50%;  : .center }

___
