# Jeu de mémoire sur les nombres

## Retourne les cartes pour associer un nombre et le nom de ce nombre.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/memory-wordnumber.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Pratiquer l'association de l'écriture chiffrée d'un nombre avec son écriture en lettre.




!!! info "Règle du jeu"

    Chaque carte cache soit une nombre écrit en chiffre soit une nombre écrit en lettres. Tu dois associer les deux écritures.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace et Entrée : retourner la carte sélectionnée</li></ul>




___
![](../images/memory-wordnumber.png){ width=50%;  : .center }

___
