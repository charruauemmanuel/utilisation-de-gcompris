# Suite de nombres

## Sélectionne les nombres les uns après les autres en respectant la suite numérique.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/number_sequence.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre la suite numérique.




!!! info "Règle du jeu"

    Dessine l'image en cliquant sur les points dans l'ordre indiqué.




___
![](../images/number_sequence.png){ width=50%;  : .center }

___
