# Trouve les nombres adjacents

## Trouve les nombres adjacents manquants.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/adjacent_numbers.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à ordonner les nombres.




!!! info "Règle du jeu"

    Trouve les nombres manquants et met les aux bonnes positions.




___
![](../images/adjacent_numbers.png){ width=50%;  : .center }

___
