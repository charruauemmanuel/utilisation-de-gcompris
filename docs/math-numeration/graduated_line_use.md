# Trouve le nombre sur une ligne graduée

## Place les valeurs sur la ligne graduée.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-numeration/graduated_line_use.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à placer des nombres sur une ligne graduée.




!!! info "Règle du jeu"

    Utilise les flèches pour déplacer le curseur à la position correspondante à la valeur sur la ligne graduée.<br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches gauche et droite : déplacer le curseur</li><li>Espace ou Entrée : valider ta réponse</li></ul>




___
![](../images/graduated_line_use.png){ width=50%;  : .center }

___
