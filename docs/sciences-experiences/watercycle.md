# Cycle de l'eau

## Tux revient d'une partie de pêche sur son bateau. Remet en marche le système d'arrivée d'eau pour qu'il puisse prendre une douche.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/watercycle.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre le cycle de l'eau.




!!! info "Règle du jeu"

    Clique sur les différents éléments actifs : le Soleil, les nuages, la station de pompage et l'usine de traitement des eaux usées, afin de réactiver le système d'eau. Lorsque le système refonctionne et que Tux est dans la douche, appuie sur la douche pour qu'il se lave.




___
![](../images/watercycle.png){ width=50%;  : .center }

___
