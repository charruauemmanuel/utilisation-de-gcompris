# Fais connaissance avec les animaux de la ferme

## Apprends à connaître les animaux de ferme, ce qu'ils font, et des choses intéressantes à leur sujet.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/explore_farm_animals.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à associer les cris des animaux à leur nom et à leur aspect.




!!! info "Règle du jeu"

    Il y a trois niveaux dans ce jeu.<br><br>Au premier niveau, les joueurs font connaissance avec chaque animal à l'écran. Clique sur le point d'interrogation et apprends à connaître l'animal, son nom et son aspect. Étudie bien ces informations, car elles te seront demandées aux niveaux 2 et 3.<br><br>Au deuxième niveau, un son correspondant à un animal est joué et tu dois trouver quel animal produit ce son. Clique sur l'animal correspondant. Si tu veux entendre à nouveau le son, appuie sur le bouton pour le rejouer.<br><br>Au troisième niveau, un texte aléatoire s'affiche et tu dois cliquer sur l'animal qui correspond au texte.




___
![](../images/explore_farm_animals.png){ width=50%;  : .center }

___
