# Mélange des couleurs de la lumière

## Découvrir le mélange des couleurs de la lumière.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/color_mix_light.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Mélange les couleurs primaires pour obtenir la couleur demandée.




!!! info "Règle du jeu"

    Cette activité enseigne comment fonctionnent les mélanges de couleurs primaires (mélanges additifs).<br><br>Modifie la couleur en bougeant les curseurs sur les lampes ou en cliquant sur le bouton + et le bouton -. Puis clique sur le bouton « Ok » pour confirmer ta réponse.<br><br>Dans le cas de la lumière, c'est juste l'opposé du mélange des couleurs en peinture ! Plus tu ajoutes de lumière, plus la couleur résultante est claire. Les couleurs primaires de la lumière sont le rouge, le vert et le bleu.




___
![](../images/color_mix_light.png){ width=50%;  : .center }

___
