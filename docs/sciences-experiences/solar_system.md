# Le système solaire

## Découvre le système solaire.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/solar_system.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Découvre le système solaire. Si tu veux en savoir plus, essaie de télécharger KStars (https://edu.kde.org/kstars/) ou Stellarium (https://stellarium.org/) deux logiciels libres consacrés à l'astronomie.




!!! info "Règle du jeu"

    Clique sur une planète ou sur le Soleil pour révéler les questions. Chaque question comporte 4 propositions. L'une d'entre elles est 100 % correcte. Essaye de répondre à toutes les questions jusqu'à ce que tu obtiennes 100 % de proximité sur le compteur de proximité.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace ou Entrée : sélectionner</li><li>Échap : revenir à l'écran précédent</li><li>Tab : afficher l'indice (seulement si l'icône « indice » est visible)</li></ul>




___
![](../images/solar_system.png){ width=50%;  : .center }

___
