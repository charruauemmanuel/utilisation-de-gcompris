# Gravité

## Introduction au concept de gravité.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/gravity.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Garde ton vaisseau spatial au milieu en évitant les collisions avec les planètes ou les astéroïdes




!!! info "Règle du jeu"

    Déplace le vaisseau spatial en utilisant les flèches gauche et droite, ou avec les boutons sur l'écran de ton appareil mobile. Essaie de rester au centre de l'écran et d'anticiper en prenant compte de la taille et de la direction de la flèche montrant la force de gravité.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches gauche et droite : déplacer le vaisseau</li></ul>




___
![](../images/gravity.png){ width=50%;  : .center }

___
