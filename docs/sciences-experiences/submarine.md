# Pilote un sous{-}marin

## Conduis le sous-marin jusqu'à l'arrivée.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/submarine.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à contrôler un sous-marin.




!!! info "Règle du jeu"

    Contrôle les différents éléments du sous-marin (le moteur, les citernes de ballast et les gouvernails) pour arriver à l'arrivée.<br><br><b>Pour contrôler avec le clavier :</b><br><b>Moteur</b><ul><li>touche D ou flèche de droite : accélérer</li><li>touche A ou flèche de gauche : freiner</li></ul><b>Citernes de ballast</b><ul><li>touche W ou flèche du haut : remplir la citerne de ballast central</li><li>touche S ou flèche du bas : vider la citerne de ballast central</li><li>touche R : remplir la citerne de ballast de gauche</li><li>touche F : vider la citerne de ballast de gauche</li><li>touche T : remplir la citerne de ballast de droite</li><li>touche G : vider la citerne de ballast de droite</li></ul><b>Gouvernails</b><ul><li>touche + : augmenter l'angle des gouvernails</li><li>touche - : diminuer l'angle des gouvernails</li></ul>




___
![](../images/submarine.png){ width=50%;  : .center }

___
