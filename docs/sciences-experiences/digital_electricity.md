# Électronique numérique

## Créer et simuler un schéma électronique numérique.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/digital_electricity.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Créer et simuler un schéma électronique numérique et sa simulation en temps réel.




!!! info "Règle du jeu"

    Fais glisser des composants électriques à partir de la barre latérale et dépose-les dans la zone de travail.<ul><li>Pour connecter deux terminaux avec un fil, clique sur le premier terminal puis sur le second.</li><li>La simulation est mise à jour en temps réel à chacune de tes modifications.</li><li>Dans la zone de travail, tu peux déplacer les composants en les faisant glisser.</li><li>Dans la barre de côté tu peux cliquer sur l'icône « outil » pour accéder au menu « outils ».</li><li>Pour supprimer un composant ou un fil, sélectionne l'outil de suppression (l'icône croix) à partir du menu, puis sélectionne le composant ou le fil que tu veux effacer.</li><li>Pour désélectionner un terminal ou l'outil de suppression, clique sur une zone vide.</li><li>Tu peux faire tourner le composant sélectionné sur lui-même en utilisant le bouton de rotation (l'icône avec la flèche rotative) situé dans le menu outils.</li><li>Tu peux obtenir des informations sur le composant sélectionné en cliquant sur le bouton d'informations (l'icône affichant la lettre I) situé dans le menu outils.</li><li>Tu peux zoomer ou dé-zoomer sur la zone de travail en utilisant les touches + et -, en utilisant les boutons de zoom du menu ou en utilisant la fonctionnalité pincer et zoomer de l'écran tactile.</li><li>Tu peux déplacer la zone de travail en cliquant sur une zone vide et en la faisant glisser.</li><li>Tu peux cliquer sur un interrupteur pour l'ouvrir ou le fermer.</li></ul>




___
![](../images/digital_electricity.png){ width=50%;  : .center }

___
