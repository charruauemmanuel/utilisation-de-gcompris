# Les nombres binaires

## Cette activité va t'apprendre à convertir un nombre décimal en nombre binaire.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/binary_bulb.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Découvrir la notion de système binaire.




!!! info "Règle du jeu"

    Allume les bonnes ampoules pour afficher la valeur binaire correspondant au nombre décimal donné. Appuie sur le bouton « Ok » lorsque tu as terminé.




___
![](../images/binary_bulb.png){ width=50%;  : .center }

___
