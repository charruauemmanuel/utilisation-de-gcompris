# Électronique analogique

## Créer et simuler un schéma électronique analogique.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/analog_electricity.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Créer un schéma électronique analogique avec sa simulation en temps réel.




!!! info "Règle du jeu"

    Fais glisser les composants à partir de la zone de sélection et dépose les dans la zone de travail. Dans cette dernière tu peux aussi déplacer les composants en les faisant glisser. Pour supprimer un composant ou un fil, sélectionne l'outil de suppression situé au-dessus du sélectionneur de composants, puis sélectionne le composant ou le fil. Tu peux cliquer sur le composant puis sur le bouton de rotation pour le faire tourner sur lui-même, ou alors sur le bouton d'informations pour accéder à des renseignements sur le composant. Tu peux cliquer sur l'interrupteur pour l'ouvrir ou le fermer. De la même façon, tu peux modifier la valeur du rhéostat en déplaçant son curseur. Pour connecter deux terminaux, clique sur le premier, puis sur le second. Pour désélectionner un terminal, clique sur une zone vide. Pour réparer une diode électroluminescente ou une ampoule défectueuse, clique dessus après l'avoir déconnectée du circuit. La simulation est mise à jour en temps réel à chacune de tes modifications.




___
![](../images/analog_electricity.png){ width=50%;  : .center }

___
