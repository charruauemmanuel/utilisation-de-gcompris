# Fais connaissance avec les animaux du monde

## Apprends à connaître les animaux du monde, les faits intéressants les concernant et leur localisation sur une carte.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/explore_world_animals.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Documente-toi sur différents animaux du monde et mémorise leur lieu de vie.




!!! info "Règle du jeu"

    Cette activité comporte deux niveaux.<br><br>Au premier niveau, les joueurs font connaissance avec chaque animal à l'écran. Clique sur le point d'interrogation et apprends à connaître l'animal, son nom et son aspect. Étudie bien ces informations, car elles te seront demandées au niveau 2.<br><br>Au second niveau, un texte aléatoire s'affiche et tu dois cliquer sur l'animal qui correspond au texte. Quand tu as correctement attribué tous les textes aux animaux correspondants, tu as gagné.




___
![](../images/explore_world_animals.png){ width=50%;  : .center }

___
