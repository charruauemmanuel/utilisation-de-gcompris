# Atterrissage sans risques

## Conduis le vaisseau jusqu'à la plate-forme d'atterrissage verte.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/land_safe.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Comprendre l'accélération due à la gravité.




!!! info "Règle du jeu"

    L'accélération causée par la gravité subit par un vaisseau spatial est proportionnelle à la masse de la planète et inversement proportionnelle au carré de la distance entre le vaisseau et le centre de la planète. Par conséquent, pour chaque planète l'accélération sera différente et plus le vaisseau s'approchera de la planète plus l'accélération augmentera.<br><br>Pour le premier niveau, utilise les flèches haut/bas pour contrôler la poussée et les flèches droite/gauche pour contrôler la direction. Sur un écran tactile tu peux contrôler la fusée en utilisant les boutons situés sur l'écran.<br><br>Pour les niveaux supérieurs, tu peux utiliser les flèches droite/gauche pour faire tourner le vaisseau sur lui-même. Pendant la rotation tu peux accélérer de façon non verticale en utilisant les flèches haut/bas.<br><br>La plate-forme d'atterrissage est verte si ta vitesse est adaptée à un atterrissage en douceur.<br><br>L'accéléromètre situé sur le côté droit montre l'accélération verticale de la fusée incluant la force gravitationnelle. Dans la zone verte l'accélération est plus importante que la force de gravitation, dans la zone rouge elle est moins importante, et dans la zone jaune, les deux forces s'annulent.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches du haut et du bas : contrôler la poussée du moteur</li><li>Flèches gauche et droite : aux premiers niveaux, se déplacer sur le côté. Aux niveaux suivants, tourner le vaisseau.</li></ul>




___
![](../images/land_safe.png){ width=50%;  : .center }

___
