# Mélange des couleurs de peinture

## Découvrir le mélange de couleurs en peinture.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/color_mix.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Mélange les couleurs primaires pour obtenir la couleur demandée.




!!! info "Règle du jeu"

    Cette activité enseigne comment fonctionnent les mélanges de couleurs primaires (mélanges soustractifs).<br><br>Modifie la couleur en bougeant les curseurs sur les lampes ou en cliquant sur le bouton + et le bouton -. Puis clique sur le bouton « Ok » pour confirmer ta réponse.<br><br>En peinture, les encres absorbent différentes couleurs de la lumière qui leur arrivent dessus, enlevant ces couleurs à ce que tu vois. Plus tu ajoutes d'encre, plus la lumière est absorbée et plus le mélange de couleurs paraît sombre. On peut en mélangeant seulement 3 couleurs primaires en obtenir beaucoup d'autres. Les couleurs primaires pour la peinture ou l'encre sont le cyan (une sorte de bleu), le magenta (une sorte de rose), et le jaune.




___
![](../images/color_mix.png){ width=50%;  : .center }

___
