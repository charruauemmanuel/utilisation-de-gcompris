# Énergie renouvelable

## Tux revient d'une partie de pêche sur son bateau. Remets le système électrique en état de marche pour qu'il ait de la lumière dans sa maison.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/renewable_energy.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends comment fonctionne un système électrique utilisant l'énergie renouvelable.




!!! info "Règle du jeu"

    Clique sur les différents éléments actifs : soleil, nuages, barrage, panneaux solaires, parc éolien et transformateurs, de façon à réactiver le système électrique dans son intégralité. Lorsque le système est remis en état de service et que Tux est dans sa maison, appuie sur le bouton de la lumière pour lui. Pour gagner, tu dois allumer tous les appareils électriques et tandis que tous les systèmes de production d'électricité fonctionnent.




___
![](../images/renewable_energy.png){ width=50%;  : .center }

___
