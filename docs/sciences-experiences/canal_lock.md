# Fonctionnement d'une écluse

## Tux a du mal à passer l'écluse en bateau. Aide-le et découvre le fonctionnement d'une écluse.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-experiences/canal_lock.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Comprendre comment fonctionne une écluse.




!!! info "Règle du jeu"

    Tu es responsable de l'écluse. Tu dois ouvrir les portes et les vannes dans le bon ordre afin de faire transiter Tux à travers l'écluse, dans un sens puis dans l'autre.




___
![](../images/canal_lock.png){ width=50%;  : .center }

___
