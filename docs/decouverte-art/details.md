# Trouve les détails

## Retrouve le lien géométrique entre les objets graphiques.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-art/details.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer le sens de l'observation et le repérage dans l'espace.




!!! info "Règle du jeu"

    Retrouve les détails du tableau affichés dans le présentoir et fais les glisser sur les points rouges pour les replacer dans le tableau.




___
![](../images/details.png){ width=50%;  : .center }

___
