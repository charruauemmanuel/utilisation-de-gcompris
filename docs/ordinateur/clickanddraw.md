# Clique et dessine

## Dessine l'image en cliquant sur les points dans l'ordre indiqué.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ordinateur/clickanddraw.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Manipuler la souris ou utiliser l'écran tactile, s'entrainer à cliquer ou appuyer sur des points précis.




!!! info "Règle du jeu"

    Dessine l'image en cliquant sur chaque point dans l'ordre. Chaque fois que tu cliques sur un point bleu, le point bleu suivant apparaît.




___
![](../images/clickanddraw.png){ width=50%;  : .center }

___
