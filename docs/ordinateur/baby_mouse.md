# La souris des débutants

## Déplace la souris ou touche l'écran puis observe le résultat.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ordinateur/baby_mouse.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Produit des sons et des effets visuels lors de l'utilisation de la souris pour aider à découvrir son utilisation.




!!! info "Règle du jeu"

    L'écran possède 3 sections :<ul><li>La colonne la plus à gauche contient 4 canards. Un clic sur l'un d'entre eux produit un son et une animation.</li><li>La zone centrale contient un canard bleu. Un déplacement du pointeur de souris ou un geste de glisser sur un écran tactile fait bouger le canard bleu.</li><li>Dans la zone des flèches, un clic sur l'une d'entre elles fait se déplacer le canard bleu dans la direction correspondante.</li></ul>Un simple clic dans la zone centrale fait apparaître un marqueur à l'endroit du clic.




___
![](../images/baby_mouse.png){ width=50%;  : .center }

___
