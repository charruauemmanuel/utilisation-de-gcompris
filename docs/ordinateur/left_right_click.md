# Entraînement aux clics de la souris

## Déplace les animaux à leur maisons en utilisant les boutons gauche et droit de ta souris.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ordinateur/left_right_click.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Utilisation de la souris. Entraînement aux clics avec les boutons droit et gauche.




!!! info "Règle du jeu"

    Un clic avec le bouton gauche sur un poisson va l'amener dans l'étang. Un clic avec le bouton droit sur un singe va l'amener dans l'arbre. Un message apparaîtra si tu n'utilises pas le bon bouton.




___
![](../images/left_right_click.png){ width=50%;  : .center }

___
