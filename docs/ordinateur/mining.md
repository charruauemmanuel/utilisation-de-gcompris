# Cherche de l'or

## Utilise la molette de la souris pour t'approcher de la falaise et chercher des pépites d'or.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ordinateur/mining.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à agrandir et réduire avec la molette de la souris ou le geste de pincement.




!!! info "Règle du jeu"

    En scrutant les roches, tu peux apercevoir des étincelles. Déplace-toi près de ces étincelles et utilise la roulette de ta souris ou le geste zoom (geste de la pince) pour zoomer. Lorsque tu atteins le niveau maximum de zoom, une pépite d'or apparaîtra à la position de l'étincelle. Clique sur la pépite pour la récolter.<br><br>Après avoir récolté la pépite, utilise la roulette de la souris ou le geste de pincement pour dé-zoomer. Une fois que tu atteins le niveau minimum du zoom, une autre pépite apparaîtra. Collecte assez de pépites pour compléter le niveau.<br><br>Le wagon situé dans le coin en bas à droite de l'écran affiche le nombre de pépites récoltées et le nombre de pépites qu'il reste à récolter pour le niveau en cours.




___
![](../images/mining.png){ width=50%;  : .center }

___
