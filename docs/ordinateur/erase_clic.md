# Clique ou tape

## Clique ou tape sur les blocs pour les effacer et découvrir l'image de fond.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ordinateur/erase_clic.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer la coordination motrice.




!!! info "Règle du jeu"

    Clique ou tape sur les rectangles pour les faire disparaître.




___
![](../images/erase_clic.png){ width=50%;  : .center }

___
