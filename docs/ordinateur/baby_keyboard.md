# Clavier basique

## Une activité simple pour découvrir le clavier.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ordinateur/baby_keyboard.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Découvrir le clavier.




!!! info "Règle du jeu"

    Saisis n'importe quelle touche du clavier.<br/>    Les lettres, les chiffres et les autres touches de caractères permettent d'afficher les caractères à l'écran.<br/>    Si un son correspondant existe, il sera joué, sinon un son de buzzer sera joué.<br/>    Les autres touches émettent un bruit de clic.<br/>    




___
![](../images/baby_keyboard.png){ width=50%;  : .center }

___
