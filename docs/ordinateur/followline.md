# Contrôle l'écoulement d'un tuyau

## Le pompier a besoin d'arrêter le feu, mais le tuyau est bouché.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ordinateur/followline.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Coordination motrice.




!!! info "Règle du jeu"

    Déplace la souris ou ton doigt sur le bouchon représenté en rouge dans le tuyau. Cela te permet de conduire l'eau jusqu'au feu. Attention, si tu sors du tuyau le bouchon recule.




___
![](../images/followline.png){ width=50%;  : .center }

___
