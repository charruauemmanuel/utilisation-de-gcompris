# Penalty

## Double-clique ou double tape sur le côté du but de foot pour marquer un but.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ordinateur/penalty.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer la coordination motrice.




!!! info "Règle du jeu"

    Double-clique ou double tape sur le ballon pour le tirer. Tu peux double-cliquer avec le bouton gauche, droit ou du milieu. Si tu n'effectues pas le geste assez rapidement Tux attrape le ballon. Tu dois alors recommencer.




___
![](../images/penalty.png){ width=50%;  : .center }

___
