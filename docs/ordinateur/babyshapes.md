# Associations géométriques

## Trouve la bonne représentation géométrique.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ordinateur/babyshapes.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à associer des formes géométriques semblables.




!!! info "Règle du jeu"

    Complète l'activité en faisant glisser les objets depuis le présentoir situé sur la gauche vers leur ombre.




___
![](../images/babyshapes.png){ width=50%;  : .center }

___
