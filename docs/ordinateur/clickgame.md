# Clique sur les poissons

## Clique sur tous les poissons avant qu'ils ne quittent l'aquarium.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ordinateur/clickgame.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Coordination motrice : déplacement précis de la main.




!!! info "Règle du jeu"

    Attrape tous les poissons en cliquant dessus ou en les touchant avec ton doigt.




___
![](../images/clickgame.png){ width=50%;  : .center }

___
