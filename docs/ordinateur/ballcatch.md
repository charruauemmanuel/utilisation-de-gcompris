# Envoie la balle à Tux

## Appuie sur les touches fléchées gauche et droite en même temps pour envoyer la balle droit devant.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ordinateur/ballcatch.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entrainer la synchronisation de la main droite et de la main gauche.




!!! info "Règle du jeu"

    Appuie sur les touches fléchées gauche et droite en même temps pour envoyer la balle droit devant. Sur un écran tactile, tu dois toucher les deux mains en même temps.




___
![](../images/ballcatch.png){ width=50%;  : .center }

___
