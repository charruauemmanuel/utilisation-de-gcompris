# Appuie deux fois ou double{-}clique

## Appuie deux fois ou double-clique sur les blocs pour les effacer et découvrir l'image de fond.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/ordinateur/erase_2clic.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer la coordination motrice.




!!! info "Règle du jeu"

    Appuie deux fois ou double-clique avec la souris sur les rectangles jusqu'à ce que tous les blocs disparaissent.




___
![](../images/erase_2clic.png){ width=50%;  : .center }

___
