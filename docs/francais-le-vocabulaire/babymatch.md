# Éléments correspondants

## Glisser et déposer les éléments pour les faire correspondre.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-le-vocabulaire/babymatch.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Coordination motrice : déplacement précis de la main.




!!! info "Règle du jeu"

    Dans la zone principale, un ensemble d'objets est présenté. Dans le présentoir (zone verticale gauche), un autre ensemble est présenté ; chacun de ces objets correspond exactement à un objet de la zone principale. Le jeu t'invite à découvrir le lien logique entre ces objets. Comment s'ajustent-ils ensemble ? Clique et fais glisser chaque objet jusqu'à l'emplacement correct dans la zone principale.




___
![](../images/babymatch.png){ width=50%;  : .center }

___
