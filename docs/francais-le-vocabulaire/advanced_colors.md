# Couleurs avancées

## Trouve le papillon de la bonne couleur.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-le-vocabulaire/advanced_colors.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à reconnaître des couleurs inhabituelles.




!!! info "Règle du jeu"

    Tu verras danser des papillons de différentes couleurs et une question. Tu dois trouver le bon papillon et le toucher.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace ou Entrée : choisir un objet</li></ul>




___
![](../images/advanced_colors.png){ width=50%;  : .center }

___
