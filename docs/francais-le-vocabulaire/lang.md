# Enrichis ton vocabulaire

## Activités d'apprentissage des langues.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-le-vocabulaire/lang.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Enrichis ton vocabulaire dans ta langue maternelle ou dans une langue étrangère.




!!! info "Règle du jeu"

    Passe en revue un ensemble de mots. Chaque mot est représenté avec une voix, un texte et une image.<br/>Lorsque ce sera fait, un exercice t'est proposé dans lequel tu dois trouver le mot associé à la voix. Dans la configuration, tu peux sélectionner la langue que tu veux apprendre.<br><br>Dans la configuration, tu peux sélectionner la langue que tu veux apprendre.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace ou Entrée : sélectionner un élément de la liste</li><li>Entrée : valider ta réponse</li><li>Tab : répéter le mot</li></ul>




___
![](../images/lang.png){ width=50%;  : .center }

___
