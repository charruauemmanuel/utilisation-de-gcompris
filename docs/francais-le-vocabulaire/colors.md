# Couleurs simples

## Clique sur la couleur énoncée oralement.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-le-vocabulaire/colors.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à reconnaître différentes couleurs.




!!! info "Règle du jeu"

    Écoute la couleur qui est prononcée et touche le canard correspondant.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace ou Entrée : sélectionne une réponse</li><li>Tab : répéter la question</li></ul>




___
![](../images/colors.png){ width=50%;  : .center }

___
