# Catégorisation

## Classe les éléments en deux groupes distincts.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-le-vocabulaire/categorization.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Construire une notion en catégorisant des éléments illustrant cette notion.




!!! info "Règle du jeu"

    Sélectionne une image ou un texte puis dépose-la dans la zone désignée par la consigne.




___
![](../images/categorization.png){ width=50%;  : .center }

___
