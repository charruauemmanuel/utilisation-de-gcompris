# Les nombres romains

## Apprends à lire et à écrire les nombres romains.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-histoire/roman_numerals.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à lire et à écrire les nombres romains et à les convertir en nombres arabes.




!!! info "Règle du jeu"

    Les nombres romains sont un système numérique inventé au temps de la Rome ancienne. L'utilisation de ce système est restée la norme à travers toute l'Europe jusqu'à la fin du Moyen Âge. Les nombres romains sont représentés par des combinaisons de lettres de l'alphabet latin.<br>Apprends les règles qui permettent de lire les nombres romains et entraîne-toi à les convertir en nombres arabes. Clique sur le bouton « Ok » pour confirmer ta réponse.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Clavier numérique : saisir les nombres arabes au clavier</li><li>Lettres : saisir les lettres romaines (I, V, X, L, C, D et M) au clavier</li><li>Entrée : pour valider tes réponses</li></ul>




___
![](../images/roman_numerals.png){ width=50%;  : .center }

___
