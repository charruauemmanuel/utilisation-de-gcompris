# La famille

## Sélectionne la relation entre les deux personnes de cette famille.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-histoire/family.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends les relations entre les membres d'une famille en utilisant le vocabulaire utilisé en Occident.




!!! info "Règle du jeu"

    Dans l'arbre généalogique les lignes blanches reliant les cercles symbolisent les relations entre deux personnes. Les personnes mariées ont deux anneaux entrelacés sur leur ligne.<br/>La personne entourée en blanc vous représente. Sélectionne la relation qui te lie avec la personne entourée en orange.<br/>




___
![](../images/family.png){ width=50%;  : .center }

___
