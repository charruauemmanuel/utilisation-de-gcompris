# L'histoire de Louis Braille

## Apprends des évènements clés de la vie de l'inventeur du système braille et rappelle-toi de leur ordre chronologique.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-histoire/louis-braille.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à se souvenir de l'ordre d'une séquence d'évènements. En savoir plus sur la vie de Louis Braille.




!!! info "Règle du jeu"

    Lis l'histoire de Louis Braille, sa biographie et l'invention du système braille. Clique sur les boutons précédent et suivant pour te déplacer d'une page à l'autre de son histoire. À la fin, tu dois placer les différents évènements de sa vie dans l'ordre chronologique.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace ou Entrée : sélectionner un élément et modifier sa position</li></ul>




___
![](../images/louis-braille.png){ width=50%;  : .center }

___
