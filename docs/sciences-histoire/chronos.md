# La chronologie

## Retrouve l'ordre des images afin de respecter une chronologie cohérente.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-histoire/chronos.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer sa capacité à reconnaître la chronologie d'une histoire.




!!! info "Règle du jeu"

    Ordonne les images afin d'obtenir une histoire cohérente. Fais glisser les pièces depuis le présentoir sur les points rouges, puis clique sur le bouton « Ok » pour valider ta réponse.




___
![](../images/chronos.png){ width=50%;  : .center }

___
