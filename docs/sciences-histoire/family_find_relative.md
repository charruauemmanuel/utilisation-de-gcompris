# Trouve les liens de parenté recherchés

## Sélectionne deux personnes correspondant au lien de parenté donné.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-histoire/family_find_relative.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends les relations entre les membres d'une famille en utilisant le vocabulaire utilisé en Occident.




!!! info "Règle du jeu"

    Un arbre généalogique est présenté ainsi que des instructions. Les cercles sont reliés par des lignes blanches pour symboliser des relations de parenté. Le mariage entre deux personnes est symbolisé par une alliance.<br/>Clique sur le nom de la relation existant entre les deux personnes sélectionnées.




___
![](../images/family_find_relative.png){ width=50%;  : .center }

___
