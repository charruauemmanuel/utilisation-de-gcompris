# Instruments de musique

## Clique sur le bon instrument de musique.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-musique/instruments.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à reconnaître les instruments de musique.




!!! info "Règle du jeu"

    Clique sur le bon instrument de musique.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace ou Entrée : choisir un objet</li><li>Tab : répéter le son de l'instrument</li></ul>




___
![](../images/instruments.png){ width=50%;  : .center }

___
