# Joue du piano

## Lis la partition puis joue la au clavier.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-musique/play_piano.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Cette activité aide à comprendre comment jouer au piano les notes présentes sur une partition.




!!! info "Règle du jeu"

    Appuie sur les touches du piano correspondant aux notes apparaissant sur la portée.<br>Les niveaux 1 à 5 permettent de travailler la clé de sol et les niveaux de 6 à 10 la clé de fa.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Espace : jouer</li><li>Touches 1 à 7 : touches blanches</li><li>F2 à F7 : touches noires</li><li>Retour arrière ou suppression : annuler</li></ul>




___
![](../images/play_piano.png){ width=50%;  : .center }

___
