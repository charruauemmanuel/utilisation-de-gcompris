# Fais connaissance avec les musiques du monde

## Apprentissage des musiques du monde.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-musique/explore_world_music.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Améliore ta connaissance de la variété des musiques du monde.




!!! info "Règle du jeu"

    Cette activité comporte trois niveaux.<br><br>Dans le premier niveau, prends plaisir à explorer des musiques provenant du monde entier. Clique sur chaque valise pour apprendre les caractéristiques de la musique de ces régions, et écoute un court extrait de celles-ci. Concentre-toi bien parce que des questions te seront posées dans les niveaux 2 et 3.<br><br>Dans le deuxième niveau tu pourras écouter des extraits musicaux, et tu dois sélectionner la provenance de la musique. Clique sur le bouton « réécouter » si tu veux réécouter l'extrait.<br><br>Dans le troisième niveau, il te faut sélectionner l'endroit correspondant à la description affichée.




___
![](../images/explore_world_music.png){ width=50%;  : .center }

___
