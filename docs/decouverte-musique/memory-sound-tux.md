# Jeu de mémoire auditive, contre Tux

## Retourne les cartes pour associer des paires de sons. Joue contre Tux.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-musique/memory-sound-tux.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entraîne ta mémoire auditive.




!!! info "Règle du jeu"

    Un groupe de cartes est affiché. Chaque carte est associée à un son et chaque son a un jumeau parfaitement identique. Clique sur une carte pour écouter le son caché, et essaie de trouver l'autre carte avec le même son. Tu ne peux retourner que deux cartes à la fois, il faut donc se rappeler de la position des sons au fur et à mesure qu'ils sont découverts. Lorsque tu retournes deux cartes identiques, elles disparaissent toutes les deux.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace et Entrée : retourner la carte sélectionnée</li></ul>




___
![](../images/memory-sound-tux.png){ width=50%;  : .center }

___
