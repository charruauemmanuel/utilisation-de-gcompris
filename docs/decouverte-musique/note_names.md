# Donne un nom à cette note

## Apprends le nom des notes, en clé de sol et en clé de fa.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-musique/note_names.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développe une bonne compréhension de la position des notes et des conventions d'écriture. Prépare pour les activités « Joue du piano » et « Composition pour piano ».




!!! info "Règle du jeu"

    Identifie les notes, joue les sur le clavier du piano et atteins un score de 100  % pour finir le niveau.




___
![](../images/note_names.png){ width=50%;  : .center }

___
