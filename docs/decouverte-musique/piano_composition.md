# Composition pour piano

## Cette activité aide à comprendre comment jouer au piano les notes présentes sur une partition.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-musique/piano_composition.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Cette activité permet de découvrir quelques éléments permettant la composition. Si tu veux en savoir plus tu peux essayer Minuet (https://minuet.kde.org/) logiciel éducatif et MuseScore (https://musescore.org/) logiciel de notation, tous les deux open source.




!!! info "Règle du jeu"

    Cette activité comporte plusieurs niveaux, chaque niveau ajoute une nouvelle fonctionnalité à la précédente.<ul><li>Niveau 1 : clavier de piano basique (comportant seulement des touches blanches) servant à expérimenter en cliquant les touches pour écrire de la musique.</li><li>Niveau 2 : le clavier affiche la clé de fa, les notes sont plus graves que lors du niveau précédent.</li><li>Niveau 3 : option permettant de choisir entre la clé de sol et la clé de fa, les touches dièses sont ajoutées.</li><li>Niveau 4 : les bémols sont ajoutés.</li><li>Niveau 5 : option pour sélectionner la durée d'une note (ronde, blanche, noire, croche, double croche).</li><li>Niveau 6 : ajout des silences (entier, demi, quart,huitième).</li><li>Niveau 7 : enregistre tes compositions et charge des mélodies prédéfinies ou précédemment enregistrées.</li></ul><b>Pour contrôler avec le clavier :</b><ul><li>Touches 1 à 7 : touches blanches</li><li>F2 à F7 : touches noires</li><li>Espace : jouer</li><li>Flèches gauche et droite : changer l'octave du clavier</li><li>Retour arrière : annuler</li><li>Effacer : si une note est sélectionnée, l'effacer sinon effacer toutes les notes</li></ul>




___
![](../images/piano_composition.png){ width=50%;  : .center }

___
