# Mélodie

## Reproduis une séquence de sons.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-musique/melody.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Activité d'entraînement de l'ouïe.




!!! info "Règle du jeu"

    Écoute la séquence de sons jouée, et répète-la en cliquant sur les barres du xylophone. Tu peux entendre à nouveau la séquence en cliquant sur le bouton de répétition.




___
![](../images/melody.png){ width=50%;  : .center }

___
