# Bats la mesure

## Apprends à suivre un rythme qui accélère.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-musique/play_rhythm.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à suivre un rythme qui accélère.




!!! info "Règle du jeu"

    Écoute le rythme joué. Lorsque tu es prêt, clique sur le tambour en suivant le rythme donné. Si tu respectes le rythme un nouveau rythme te sera présenté, sinon il faut recommencer.<br>Les niveaux impairs affichent une barre verticale sur la portée qui marque le rythme : clique sur le tambour lorsque la ligne est au milieu des notes.<br>Les niveaux pairs sont plus difficiles car il n'y a pas de barres verticales. Tu dois lire la longueur d'une note et la jouer en respectant sa durée. Tu peux également cliquer sur le métronome pour prendre référence sur le tempo d'une note noire (1 temps).<br>Clique sur le bouton Recharger si tu veux rejouer le rythme.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Espace : cliquer sur le tambour</li><li>Entrée ou Retour : rejouer le rythme</li><li>Flèches du haut et du bas : accélérer ou décélérer le tempo</li><li>Tab : démarrer ou arrêter le métronome si celui-ci est visible</li></ul>




___
![](../images/play_rhythm.png){ width=50%;  : .center }

___
