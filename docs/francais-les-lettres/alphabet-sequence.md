# Ordre alphabétique

## Déplace l'hélicoptère pour attraper les nuages en suivant l'ordre alphabétique.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-lettres/alphabet-sequence.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends l'ordre alphabétique.




!!! info "Règle du jeu"

    Attrape les lettres de l'alphabet. Avec le clavier, utilise les touches fléchées pour déplacer l'hélicoptère. Avec la souris ou sur un écran tactile, clique ou tape simplement sur l'endroit où tu veux aller. Pour savoir quelle lettre tu dois attraper, tu peux soit t'en souvenir, soit vérifier dans le coin en bas à droite.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : déplacer l'hélicoptère.</li></ul>




___
![](../images/alphabet-sequence.png){ width=50%;  : .center }

___
