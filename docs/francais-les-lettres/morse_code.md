# Découvre le code Morse international

## Communique en utilisant le code Morse.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-lettres/morse_code.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à envoyer et recevoir des messages en utilisant le code Morse international.




!!! info "Règle du jeu"

    Il t'est demandé soit d'envoyer un message en code morse, soit de convertir le message morse reçu en lettres ou en chiffres. Pour apprendre le code morse, tu peux consulter la carte de traduction contenant le code de toutes les lettres et de tous les chiffres en cliquant sur l'icône de l'appareil de code morse.




___
![](../images/morse_code.png){ width=50%;  : .center }

___
