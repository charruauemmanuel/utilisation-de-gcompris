# Ordonne les lettres de l'alphabet

## Ordonne les lettres données selon un ordre alphabétique croissant ou décroissant comme demandé.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-lettres/ordering_alphabets.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends l'ordre alphabétique.




!!! info "Règle du jeu"

    Fais glisser puis dépose les lettres dans la zone supérieure en respectant, comme demandé, l'ordre alphabétique croissant ou décroissant.




___
![](../images/ordering_alphabets.png){ width=50%;  : .center }

___
