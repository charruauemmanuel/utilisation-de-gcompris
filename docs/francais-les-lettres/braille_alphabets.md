# Découvre le système braille

## Apprends et mémorise le système braille.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-lettres/braille_alphabets.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Faire découvrir le système braille aux enfants.




!!! info "Règle du jeu"

    L'écran comporte trois sections : une cellule braille interactive, une instruction t'indiquant le caractère à reproduire, et en haut, les caractères en braille à utiliser comme une référence. Chaque niveau enseigne un ensemble de 10 caractères. Reproduis le caractère donné dans la cellule braille interactive.<br><br>Tu peux ouvrir la table de Braille en cliquant sur l'icône « Braille » bleue.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Clavier numérique : 1 à 6 sélectionner/désélectionner les points correspondants</li><li>Espace : ouvrir ou fermer le tableau Braille</li></ul>




___
![](../images/braille_alphabets.png){ width=50%;  : .center }

___
