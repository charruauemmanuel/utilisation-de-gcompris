# Jeu de mémoire sur les majuscules et minuscules, contre Tux

## Retourne les cartes et retrouve les paires de lettres minuscules et majuscules. Tu joues contre Tux.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-lettres/memory-case-association-tux.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre les lettres minuscules et majuscules.




!!! info "Règle du jeu"

    Chaque carte cache une lettre, minuscule ou majuscule. Tu dois associer les minuscules et les majuscules d'une même lettre.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace et Entrée : retourner la carte sélectionnée</li></ul>




___
![](../images/memory-case-association-tux.png){ width=50%;  : .center }

___
