# Mon premier traitement de texte

## Ecris tes premiers textes.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-lettres/baby_wordprocessor.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à écrire un texte en utilisant un traitement de texte.




!!! info "Règle du jeu"

    Appuie sur le clavier physique ou virtuel comme dans un logiciel de traitement de texte.<br/>Appuie sur le bouton « Titre » pour faire grossir le texte. De la même façon, le bouton « Sous-titre » fera apparaître le texte en un peu moins gros. Appuyer sur « Paragraphe » enlèvera la mise en forme.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer dans le texte</li><li>Maj + Flèches : sélectionner une partie du texte</li><li>Ctrl + A : sélectionner tout le texte</li><li>Ctrl + C : copier le texte sélectionné</li><li>Ctrl + X : couper le texte sélectionné</li><li>Ctrl + V : coller le texte coupé ou collé</li><li>Ctrl + D : effacer le texte sélectionné</li><li>Ctrl + Z : annuler</li><li>Ctrl + Maj + Z : refaire</li></ul>




___
![](../images/baby_wordprocessor.png){ width=50%;  : .center }

___
