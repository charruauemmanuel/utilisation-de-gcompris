# Braille ludique

## Coder en braille.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-lettres/braille_fun.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entraine-toi à écrire en braille.




!!! info "Règle du jeu"

    Compose une lettre Braille correspondant aux lettres sur la bannière tirée par Tux et son avion. Tu peux consulter le tableau Braille en cliquant sur le bouton d'aide.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Espace : ouvrir ou fermer le tableau Braille</li></ul>




___
![](../images/braille_fun.png){ width=50%;  : .center }

___
