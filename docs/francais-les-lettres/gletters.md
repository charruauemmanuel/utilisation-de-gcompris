# Jeu de lettres

## En les saisissant au clavier, élimine les lettres qui tombent avant qu'elles ne touchent le sol.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-lettres/gletters.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Associer les lettres du clavier avec les lettres présentées à l'écran.




!!! info "Règle du jeu"

    En les saisissant au clavier, élimine les lettres qui tombent avant qu'elles ne touchent le sol.




___
![](../images/gletters.png){ width=50%;  : .center }

___
