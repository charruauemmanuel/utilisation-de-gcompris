# Clique sur une lettre majuscule

## Écoute le nom d'une lettre et clique dessus.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-lettres/click_on_letter_up.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Connaître le nom des lettres majuscules.




!!! info "Règle du jeu"

    Une lettre est prononcée. Clique sur la lettre correspondante dans la zone principale. Tu peux réécouter la lettre en cliquant sur l'icône bouche.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace : sélectionner un élément</li><li>Tab : répéter la question</li></ul>




___
![](../images/click_on_letter_up.png){ width=50%;  : .center }

___
