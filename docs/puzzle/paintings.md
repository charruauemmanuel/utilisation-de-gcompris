# Assemble le puzzle

## Attrape et fais glisser les éléments pour reconstituer le tableau original.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/puzzle/paintings.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer le sens de l'observation et le repérage dans l'espace.




!!! info "Règle du jeu"

    Attrape et fais glisser les éléments pour reconstituer le tableau original




___
![](../images/paintings.png){ width=50%;  : .center }

___
