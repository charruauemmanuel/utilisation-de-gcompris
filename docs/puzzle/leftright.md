# Main droite ou main gauche~?

## Détermine si la photo présente une main droite ou gauche.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/puzzle/leftright.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Distinguer depuis différents points de vue la main gauche et la main droite. Représentation spatiale.




!!! info "Règle du jeu"

    Une main est affichée. Il faut déterminer si c'est une main gauche ou droite.<br/>Clique sur le bouton gauche ou droit en fonction de la main affichée.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèche de gauche : choisir la main gauche</li><li>Flèche de droite : choisir la main droite</li></ul>




___
![](../images/leftright.png){ width=50%;  : .center }

___
