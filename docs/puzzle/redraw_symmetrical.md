# Reproduis l'image symétrique

## Reproduis l'image symétriquement dans la grille vide.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/puzzle/redraw_symmetrical.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à repérer une position dans une grille et à reproduire un modèle symétriquement.




!!! info "Règle du jeu"

    Choisis d'abord la bonne couleur dans la barre d'outils. Puis clique sur la grille et glisse pour dessiner, et relâche le bouton pour arrêter de peindre.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Clavier numérique : sélectionner une couleur</li><li>Flèches : se déplacer dans la grille</li><li>Espace ou Entrée : peindre</li></ul>




___
![](../images/redraw_symmetrical.png){ width=50%;  : .center }

___
