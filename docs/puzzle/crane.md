# Reconstruis le même modèle

## Pilote le bras de la grue pour déplacer les éléments suivant le modèle.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/puzzle/crane.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Coordination motrice.




!!! info "Règle du jeu"

    Déplace les éléments dans le cadre bleu afin de refaire le modèle de droite. A côté de la grue, tu trouveras 4 flèches pour bouger ces éléments. Pour sélectionner les éléments il suffit d'utiliser ces flèches. Si tu préfères tu peux utiliser les flèches du clavier, la barre d'espace ou la touche de tabulation. Sur la version mobile, tu peux faire glisser les éléments pour les faire passer dans le cadre de gauche.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : déplacer l'objet sélectionné</li><li>Espace, Retour ou Tabulation : sélectionner l'élément suivant</li></ul>




___
![](../images/crane.png){ width=50%;  : .center }

___
