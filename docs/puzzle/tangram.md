# Le jeu du Tangram

## Déplace les objets pour reproduire l'assemblage donné.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/puzzle/tangram.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à se repérer dans l'espace, à mémoriser une position et à reproduire un modèle.




!!! info "Règle du jeu"

    Déplace une pièce en la faisant glisser. Le bouton de symétrie apparaît sur les éléments qui le prennent en charge. Clique sur le bouton de rotation ou fais glisser ton doigt autour de la pièce pour la faire tourner si nécessaire. Pour débuter, des objets plus simples à assembler sont disponibles dans l'activité « Puzzle simple ».




___
![](../images/tangram.png){ width=50%;  : .center }

___
