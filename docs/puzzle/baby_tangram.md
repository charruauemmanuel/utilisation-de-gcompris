# Puzzle simple

## Place les objets sur leur forme.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/puzzle/baby_tangram.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    S'entrainer à associer un objet à sa forme.




!!! info "Règle du jeu"

    Déplace une pièce en la faisant glisser. Clique sur le bouton de rotation ou fais glisser ton doigt autour de la pièce pour la faire tourner si nécessaire. Des formes plus compliquées à assembler peuvent être trouvées dans l'activité du jeu du Tangram.




___
![](../images/baby_tangram.png){ width=50%;  : .center }

___
