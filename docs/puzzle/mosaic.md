# Reconstruis la mosaïque

## Place chaque élément au même endroit, comme dans l'exemple donné

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/puzzle/mosaic.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entraîner ses compétences d'observation, de reproduction et de mise en correspondance.




!!! info "Règle du jeu"

    Choisis d'abord l'objet que tu veux mettre puis clique sur une case vide pour l'y déposer.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer dans la zone</li><li>Espace ou Entrée : sélectionner ou placer un élément</li><li>Tab : naviguer entre la liste des éléments et la mosaïque</li></ul>




___
![](../images/mosaic.png){ width=50%;  : .center }

___
