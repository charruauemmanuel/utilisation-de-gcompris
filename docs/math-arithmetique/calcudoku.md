# Calcudoku

## Résous le calcudoku.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/calcudoku.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entraîne tes capacités de raisonnement logique, de positionnement spatial et de déduction en utilisant le calcul.




!!! info "Règle du jeu"

    Sélectionne un nombre dans la liste et clique sur la position où tu veux l'ajouter sur la grille. Chaque nombre ne doit apparaître qu'une seule fois sur une même ligne ou colonne. Les cages sont des groupes de cellules dans lesquelles une information est donnée pour aider au remplissage. Les cages composées de plus d'une case ont un résultat et un opérateur : il faut atteindre le résultat en combinant tous les nombres de la cage avec l'opérateur donné. Les cages ne contenant qu'une seule case fournissent le nombre à saisir.




___
![](../images/calcudoku.png){ width=50%;  : .center }

___
