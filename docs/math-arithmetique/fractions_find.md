# Trouve les fractions

## Trouvez le numérateur et le dénominateur de la fraction représentée.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/fractions_find.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à écrire une fraction correspondant à une représentation fractionnaire donnée.




!!! info "Règle du jeu"

    Écris la fraction correspondant correspondant à la représentation fractionnaire : compte le nombre total de parts de l'unité représentée et écris le comme dénominateur. Puis compte le nombre de parts sélectionnées et entre le comme numérateur.




___
![](../images/fractions_find.png){ width=50%;  : .center }

___
