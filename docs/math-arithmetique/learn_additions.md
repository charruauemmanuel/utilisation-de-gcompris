# Apprends les additions

## Apprends les additions avec des petits nombres.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/learn_additions.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre les additions en ajoutant des quantités.




!!! info "Règle du jeu"

    Une addition est affichée. Calcule le résultat, remplis le nombre correspondant de cercles et confirme ta réponse.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace : sélectionner ou désélectionner un cercle.</li><li>Entrée : pour valider tes réponses</li></ul>




___
![](../images/learn_additions.png){ width=50%;  : .center }

___
