# Jeu de mémoire sur la multiplication et la division contre Tux

## En jouant avec Tux, retourne les cartes pour associer une multiplication ou une division avec son résultat.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/memory-math-mult-div-tux.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Pratiquer les tables de multiplication et de division.




!!! info "Règle du jeu"

    Chaque carte cache soit une opération (une multiplication ou une division) soit un résultat. Tu dois associer les opérations avec leur résultat.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace et Entrée : retourner la carte sélectionnée</li></ul>




___
![](../images/memory-math-mult-div-tux.png){ width=50%;  : .center }

___
