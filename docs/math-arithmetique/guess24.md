# Trouve 24

## Calcule pour trouver 24.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/guess24.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entraîne-toi à calculer avec quatre opérateurs.




!!! info "Règle du jeu"

    Combine les 4 nombres avec les opérations données pour trouver 24.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer dans les nombres et les opérateurs</li><li>Espace ou Entrée : sélectionner ou désélectionner le nombre ou l'opérateur actuel</li><li>Touche opérateur (+, -, *, /) : sélectionner l'opérateur</li><li>Retour arrière ou suppression : annuler la dernière opération</li><li>Tabulation : bascule entre les nombres et les opérateurs</li></ul>




___
![](../images/guess24.png){ width=50%;  : .center }

___
