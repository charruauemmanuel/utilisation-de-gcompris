# Le chapeau du magicien

## Compte les étoiles qui se glissent sous le chapeau magique.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/magic-hat-plus.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à additionner.




!!! info "Règle du jeu"

    Clique sur le chapeau pour le soulever. Des étoiles se glissent dedans. En tout, combien d'étoiles as-tu vu y entrer ? Compte attentivement. Clique sur les étoiles dans la zone du bas pour représenter ta réponse et appuie sur le bouton « Ok » pour valider.




___
![](../images/magic-hat-plus.png){ width=50%;  : .center }

___
