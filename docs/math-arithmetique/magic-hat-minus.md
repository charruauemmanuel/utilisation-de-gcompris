# Le chapeau du magicien

## Compte les objets présents sous le chapeau magique.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/magic-hat-minus.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à soustraire.




!!! info "Règle du jeu"

    Clique sur le chapeau pour le soulever. Des étoiles se glissent dessous mais quelques unes s'échappent. En tout, combien d'étoiles y a-t-il encore sous le chapeau ? Compte attentivement. Clique sur les étoiles dans la zone du bas pour représenter ta réponse. Clique sur le bouton « Ok » une fois que tu as fini.




___
![](../images/magic-hat-minus.png){ width=50%;  : .center }

___
