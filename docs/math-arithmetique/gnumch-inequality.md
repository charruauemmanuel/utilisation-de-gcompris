# Le croqueur d'inégalités

## Guide le croqueur de nombres sur toutes les expressions qui ne sont pas égales au nombre présenté en bas de l'écran.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/gnumch-inequality.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entraînement aux opérations d'addition, de soustraction, de multiplication et de division.




!!! info "Règle du jeu"

    Si tu as un clavier, tu peux utiliser les touches flèches pour te déplacer et appuyer sur la barre d'espace pour avaler les nombres. Avec la souris, tu peux cliquer sur un bloc à côté de ta position pour te déplacer et cliquer encore pour avaler les nombres. Avec un écran tactile, tu peux faire comme avec une souris ou bien glisser le doigt dans la direction dans laquelle tu veux aller, et appuyer pour avaler les nombres.<br><br>Fais attention pour éviter les Troggles.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace : avaler les nombres</li></ul>




___
![](../images/gnumch-inequality.png){ width=50%;  : .center }

___
