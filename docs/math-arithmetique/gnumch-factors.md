# Le croqueur de facteurs

## Guide le croqueur de nombres sur tous les diviseurs du nombre présenté en bas de l'écran.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/gnumch-factors.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à reconnaître les multiples et les diviseurs.




!!! info "Règle du jeu"

    Les facteurs d'un nombre sont tous les nombres qui divisent ce nombre en parts égales. Par exemple, les facteurs de 6 sont 1, 2, 3 et 6. 4 n'est pas un facteur de 6 car 6 n'est pas divisible en 4 parts égales. Si un nombre est un multiple d'un second nombre alors ce second nombre est un facteur du premier. Tu peux penser aux multiples comme des familles, et les facteurs sont les gens dans ces familles. Ainsi, 1, 2, 3 et 6 appartiennent tous à la famille 6 mais 4 appartient à une autre famille.<br><br>Si tu as un clavier, tu peux utiliser les touches flèches pour te déplacer et appuyer sur la barre d'espace pour avaler les nombres. Avec la souris, tu peux cliquer sur un bloc à côté de ta position pour te déplacer et cliquer encore pour avaler les nombres. Avec un écran tactile, tu peux faire comme avec une souris ou bien glisser le doigt dans la direction dans laquelle tu veux aller, et appuyer pour avaler les nombres.<br><br>Fais attention pour éviter les Troggles.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace : avaler les nombres</li></ul>




___
![](../images/gnumch-factors.png){ width=50%;  : .center }

___
