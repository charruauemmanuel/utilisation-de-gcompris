# Utilise les compléments à 10

## Utilise les compléments à 10 pour simplifier une opération.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/tens_complement_use.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends une utilisation futée du complément à dix.




!!! info "Règle du jeu"

    Décompose les additions pour créer des paires de nombres égales à dix entre chaque parenthèse. Sélectionne un nombre dans la liste, puis sélectionne un emplacement vide d'une opération pour y déplacer le nombre sélectionné.<br>Lorsque toutes les lignes sont remplies, appuie sur le bouton Ok pour valider les réponses. Si certaines réponses sont incorrectes, une icône en forme de croix apparaîtra sur les lignes correspondantes. Pour corriger les erreurs, clique sur les mauvaises réponses pour les supprimer et répète les étapes précédentes.




___
![](../images/tens_complement_use.png){ width=50%;  : .center }

___
