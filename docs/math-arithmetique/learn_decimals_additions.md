# Additions avec des nombres décimaux

## Apprends à additionner avec des nombres décimaux.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/learn_decimals_additions.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à additionner avec des chiffres décimaux en comptant combien de carrés (dixièmes) sont nécessaires pour représenter le résultat.




!!! info "Règle du jeu"

    Une addition avec deux nombres décimaux est affichée. Fais glisser la flèche pour sélectionner une partie de la barre et fais glisser la partie sélectionnée de la barre vers la zone vide. Répète ces étapes jusqu'à ce que le nombre de barres déposées corresponde au résultat de l'addition. Enfin, clique sur le bouton « Ok » pour valider ta réponse.<br><br>Si la réponse est correcte, saisis le résultat correspondant et clique sur le bouton « Ok » pour valider ta réponse.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Entrée : pour valider tes réponses</li><li>Nombres : saisis le résultat</li></ul>




___
![](../images/learn_decimals_additions.png){ width=50%;  : .center }

___
