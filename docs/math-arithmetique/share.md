# Partage les confiseries

## Essaye de séparer les confiseries de façon équitable entre un nombre d'enfants donné

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/share.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à partager, pratiquer la division.




!!! info "Règle du jeu"

    Suis les instructions affichées à l'écran : place en premier le bon nombre de garçons et filles dans la zone centrale puis déplace les confiseries dans chaque case d'enfants.<br>S'il y a un reste, il doit être placé dans la boîte à confiseries.




___
![](../images/share.png){ width=50%;  : .center }

___
