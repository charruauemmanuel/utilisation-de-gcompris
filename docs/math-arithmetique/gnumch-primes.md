# Le croqueur de nombres premiers

## Guide le croqueur de nombres sur tous les nombres premiers.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/gnumch-primes.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à reconnaître les nombres premiers.




!!! info "Règle du jeu"

    Les nombres premiers sont des nombres qui ne sont divisibles que par eux-mêmes et par 1. Par exemple 3 est un nombre premier, mais 4 n'en est pas un, car 4 est divisible par 2. Tu peux penser aux nombres premiers comme une toute petite famille : elle ne contient toujours que deux personnes ! Uniquement le nombre lui-même et 1. Un autre exemple, 5 fait partie de ces nombres uniques, sa famille contient seulement 5 * 1 = 5 et 1. Par contre 6 n'est pas un nombre premier, la famille contiendrait 6 * 1 = 6, 3 * 2 = 6 et 1, ce qui fait plus que le nombre lui-même et 1.<br><br>Si tu as un clavier, tu peux utiliser les touches flèches pour te déplacer et appuyer sur la barre d'espace pour avaler les nombres. Avec la souris, tu peux cliquer sur un bloc à côté de ta position pour te déplacer et cliquer encore pour avaler les nombres. Avec un écran tactile, tu peux faire comme avec une souris ou bien glisser le doigt dans la direction dans laquelle tu veux aller, et appuyer pour avaler les nombres.<br><br>Fais attention pour éviter les Troggles.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace : avaler les nombres</li></ul>




___
![](../images/gnumch-primes.png){ width=50%;  : .center }

___
