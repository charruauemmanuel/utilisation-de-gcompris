# Permute pour obtenir des compléments à 10

## Permuter les chiffres pour créer des paires égales à dix.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/tens_complement_swap.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à utiliser les compléments à dix pour optimiser l'ordre des nombres dans une opération.




!!! info "Règle du jeu"

    Crée des paires de nombres égales à dix à l'intérieur de chaque parenthèse. Sélectionne un nombre, puis sélectionne un autre nombre de la même opération pour intervertir leurs positions. Lorsque toutes les lignes sont remplies, appuie sur le bouton Ok pour valider les réponses. Si certaines réponses sont incorrectes, une icône en forme de croix apparaît sur les lignes correspondantes. Corrige les erreurs, puis appuie à nouveau sur le bouton Ok.




___
![](../images/tens_complement_swap.png){ width=50%;  : .center }

___
