# Additions avec un jeu de fléchettes

## Envoie des fléchettes sur la cible et calcule ton score.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/target.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Pratiquer les additions de plusieurs nombres.




!!! info "Règle du jeu"

    Pour commencer, vérifie la vitesse et la direction de la cible et clique dessus pour lancer une flèche. Quand toutes tes flèches sont lancées, on te demande de comptabiliser ton score. Saisis le score avec le clavier.




___
![](../images/target.png){ width=50%;  : .center }

___
