# Divisions

## Entraînement à la division.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/algebra_div.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    En temps limité, trouve le résultat de la division de deux nombres.




!!! info "Règle du jeu"

    Une division est affichée à l'écran. Trouve rapidement le résultat et utilise le clavier de ton ordinateur ou le clavier tactile pour le saisir. Tu dois être rapide et donner ta réponse avant que les manchots dans leur ballon n'atterrissent !<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Clavier numérique : saisir ta réponse</li><li>Touche retour arrière : effacer le dernier chiffre de ta réponse</li><li>Entrée : pour valider tes réponses</li></ul>




___
![](../images/algebra_div.png){ width=50%;  : .center }

___
