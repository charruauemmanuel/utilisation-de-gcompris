# Représente les fractions

## Sélectionne le nombre de parts indiqué dans les instructions.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/fractions_create.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à représenter une fraction donnée d'une forme.




!!! info "Règle du jeu"

    Une forme divisée en parts égales est affichée. Sélectionne le nombre approprié de parts pour représenter la fraction donnée.




___
![](../images/fractions_create.png){ width=50%;  : .center }

___
