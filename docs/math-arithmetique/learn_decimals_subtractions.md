# Soustractions avec des nombres décimaux

## Apprends à soustraire avec des nombres décimaux.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/learn_decimals_subtractions.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à soustraire avec des chiffres décimaux en comptant combien de carrés doivent être soustraits pour représenter le résultat.




!!! info "Règle du jeu"

    Une soustraction avec deux nombres décimaux est affichée. Le premier nombre de la soustraction est représenté par des barres. Chaque barre représente une unité complète et chaque carré de la barre représente un dixième de cette unité. Clique sur les carrés pour soustraire le second nombre et présente le résultat de l'opération.<br><br>Si la réponse est correcte, saisis le résultat correspondant et clique sur le bouton « Ok » pour valider ta réponse.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Entrée : pour valider tes réponses</li><li>Nombres : saisis le résultat</li></ul>




___
![](../images/learn_decimals_subtractions.png){ width=50%;  : .center }

___
