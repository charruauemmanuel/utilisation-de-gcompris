# Le croqueur de multiples

## Guide le croqueur de nombres sur tous les multiples du nombre présenté en bas de l'écran.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/gnumch-multiples.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à reconnaître les multiples et les diviseurs.




!!! info "Règle du jeu"

    Les multiples d'un nombre sont tous les nombres qui sont égaux au nombre original, multiplié par un autre nombre. Par exemple, 24, 36, 48 et 60 sont tous multiples de 12. 25 n'est pas un multiple de 12 parce qu'il n'y a aucun nombre qui peut être multiplié par 12 pour obtenir 25. Si un nombre est un facteur d'un second nombre alors ce second nombre est un multiple du premier nombre. Tu peux penser aux multiples comme des familles, et les facteurs sont les gens dans ces familles. Le facteur 5 a les parents 10, les grands parents 15, les arrière grands parents 20, les arrière arrière grands parents 25, et pour chaque nouveau saut de 5, tu peux ajouter un « arrière » devant ! Mais le nombre 5 n'appartient pas aux familles 8 ou 23. Tu ne peux mettre un nombre de fois 5 dans 8 ou 23 sans qu'il ne reste rien. Donc 8 n'est pas un multiple de 5, 23 non plus. Seuls 5, 10, 15, 20, 25… sont des multiples (ou des familles ou des pas) de 5.<br><br>Si tu as un clavier, tu peux utiliser les touches flèches pour te déplacer et appuyer sur la barre d'espace pour avaler les nombres. Avec la souris, tu peux cliquer sur un bloc à côté de ta position pour te déplacer et cliquer encore pour avaler les nombres. Avec un écran tactile, tu peux faire comme avec une souris ou bien glisser le doigt dans la direction dans laquelle tu veux aller, et appuyer pour avaler les nombres.<br><br>Fais attention pour éviter les Troggles.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace : avaler les nombres</li></ul>




___
![](../images/gnumch-multiples.png){ width=50%;  : .center }

___
