# Le croqueur d'égalités

## Guide le croqueur de nombres sur les expressions qui sont égales au nombre présenté en bas de l'écran.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/gnumch-equality.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entraînement aux opérations d'addition, de multiplication, de division et de soustraction.




!!! info "Règle du jeu"

    Guide le croqueur de nombres sur les expressions qui sont égales au nombre présenté en bas de l'écran.<br><br>Si tu as un clavier, tu peux utiliser les touches flèches pour te déplacer et appuyer sur la barre d'espace pour avaler les nombres. Avec la souris, tu peux cliquer sur un bloc à côté de ta position pour te déplacer et cliquer encore pour avaler les nombres. Avec un écran tactile, tu peux faire comme avec une souris ou bien glisser le doigt dans la direction dans laquelle tu veux aller, et appuyer pour avaler les nombres.<br><br>Fais attention pour éviter les Troggles.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace : avaler les nombres</li></ul>




___
![](../images/gnumch-equality.png){ width=50%;  : .center }

___
