# Devine l'opération

## Trouvez les opérations pour obtenir le résultat requis.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/math-arithmetique/guesscount.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    S'entrainer à calculer en ligne.




!!! info "Règle du jeu"

    Sélectionne les bons nombres et opérateurs dans les boîtes pour obtenir le nombre donné dans les instructions.




___
![](../images/guesscount.png){ width=50%;  : .center }

___
