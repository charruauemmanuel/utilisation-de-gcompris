# Découvre les monuments célèbres

## Explorer les monuments célèbres du monde.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-geography/explore_monuments.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends des informations sur différents monuments du monde et apprends leur position géographique.




!!! info "Règle du jeu"

    Appuie sur les différents points pour te documenter sur les différents monuments et trouve où ces monuments sont situés sur la carte à l'aide de leur nom.




___
![](../images/explore_monuments.png){ width=50%;  : .center }

___
