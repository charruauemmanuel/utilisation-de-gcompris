# Repère l'emplacement de la région

## Fais glisser et dépose les régions sur les cartes de pays.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-geography/geo-country.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à localiser et à placer sur la carte les régions des pays.




!!! info "Règle du jeu"

    Fais glisser et dépose les régions vers leur emplacement correct pour reconstituer la carte.




___
![](../images/geo-country.png){ width=50%;  : .center }

___
