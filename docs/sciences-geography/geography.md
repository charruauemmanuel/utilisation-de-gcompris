# Localise les pays

## Place les continents et les pays sur la carte du monde.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/sciences-geography/geography.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à localiser et à placer sur la carte les continents et les pays.




!!! info "Règle du jeu"

    Fais glisser et dépose les éléments de la carte vers l'emplacement correct pour reconstituer la carte.




___
![](../images/geography.png){ width=50%;  : .center }

___
