# Sudoku

## Résous le sudoku.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/sudoku.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer ses compétences liées au raisonnement logique : mise en relation de données, déduction et repérage dans l'espace.




!!! info "Règle du jeu"

    Sélectionne un nombre ou un symbole et clique sur une case pour l'y ajouter. Chaque symbole ne doit apparaître qu'une seule fois par rangée, par colonne et par sous-région s'il y en a. Si une action n'est pas permise, le jeu vous indiquera pourquoi. Mais faites attention : ce n'est pas parce-que vous pouvez saisir un nombre ou un symbole dans une case que ce sera forcément correct pour résoudre le Sudoku complet.




___
![](../images/sudoku.png){ width=50%;  : .center }

___
