# Assemble le train

## Reproduis la maquette du train en haut de l'écran.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/railroad.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Pour entraîner la mémoire.




!!! info "Règle du jeu"

    Un train, composé d'une locomotive et de wagons, est présenté pendant quelques secondes en haut de l'écran. Reproduis-le en faisant glisser les wagons et la locomotive en haut de l'écran. Pour enlever un élément fais le glisser vers le bas.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer dans la zone avec les wagons ou celle de la réponse</li><li>Espace : ajouter un élément à partir des parties du train à la zone de réponse, ou inverse deux éléments dans la zone de réponse</li><li>Suppression ou Retour arrière : enlever l'élément sélectionné de la zone de réponse</li><li>Entrée ou Retour : donner ta réponse</li></ul>




___
![](../images/railroad.png){ width=50%;  : .center }

___
