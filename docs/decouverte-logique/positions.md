# Positions

## Trouve la position de l'enfant par rapport à la boîte.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/positions.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Décris la position relative d'un objet.




!!! info "Règle du jeu"

    Différentes images montrant une boîte et un enfant te sont présentées. Il faut que tu retrouves la position de l'enfant par rapport à la boîte puis que tu sélectionnes la bonne réponse.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>« Espace » ou « Entrée » : pour valider la réponse sélectionnée</li></ul>




___
![](../images/positions.png){ width=50%;  : .center }

___
