# Coloration de graphe

## Mets des couleurs aux nœuds du graphe de telle façon que deux nœuds côte-à-côte n'aient pas la même couleur.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/graph-coloring.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer son sens de la stratégie, apprendre à distinguer les couleurs et à mémoriser les positions relatives.




!!! info "Règle du jeu"

    Place les couleurs/formes sur le graphe de telle sorte que deux nœuds adjacents n'aient pas la même couleur. Sélectionne un nœud, puis sélectionne un élément dans la liste pour le placer sur le nœud.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches gauche et droite : se déplacer</li><li>Espace : sélectionner un élément</li></ul>




___
![](../images/graph-coloring.png){ width=50%;  : .center }

___
