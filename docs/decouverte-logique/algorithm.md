# Associations logiques

## Complète la liste de fruits.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/algorithm.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Associations logiques




!!! info "Règle du jeu"

    Regarde les deux listes. Chaque fruit de la première liste a été remplacé par un autre fruit de la seconde liste. Regarde bien le motif, et complète cette seconde liste en utilisant les bons fruits.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace ou Entrée : choisir un objet</li></ul>




___
![](../images/algorithm.png){ width=50%;  : .center }

___
