# Le jeu de taquin

## Déplace chaque élément pour recréer l'image.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/fifteen.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer l'anticipation et le repérage dans l'espace.




!!! info "Règle du jeu"

    Clique ou glisse une pièce jouxtant l'espace libre ; elle s'y déplacera en libérant son espace.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : déplace une pièce sur l'emplacement vide.</li></ul>




___
![](../images/fifteen.png){ width=50%;  : .center }

___
