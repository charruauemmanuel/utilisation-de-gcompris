# Encode le parcours de Tux (relatif)

## Déplace Tux le long du chemin pour atteindre la cible.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/path_encoding_relative.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer ses compétences en visualisation et lecture de plan, et apprendre à suivre des directions.




!!! info "Règle du jeu"

    Utilise les boutons de flèches pour déplacer Tux le long du chemin jusqu'à ce qu'il atteigne la cible.<br><br>Les directions sont relatives, selon l'orientation actuelle de Tux.<br><br>Cela signifie que « Vers le haut » réalise un déplacement vers l'avant, « Vers le bas » vers l'arrière, « Vers la gauche » vers le côté gauche de Tux et « Vers la droite » vers le côté droit de Tux.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : directions</li></ul>




___
![](../images/path_encoding_relative.png){ width=50%;  : .center }

___
