# La frise

## Reproduis et termine la frise.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/frieze.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends les algorithmes.




!!! info "Règle du jeu"

    Reproduis la frise du haut. Dans certains niveaux, tu devras soit compléter la frise, soit la reproduire après l'avoir mémorisée.<br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches gauche et droite : choisir une forme</li><li>Espace : ajouter la forme à la frise</li><li>Retour arrière ou suppression : retirer la dernière forme de la frise</li><li>Entrée : pour valider tes réponses</li><li>Tab : basculer entre l'édition de la frise et la vue du modèle</li></ul>




___
![](../images/frieze.png){ width=50%;  : .center }

___
