# Décode le chemin

## Suivre les directions données pour aider Tux à atteindre la cible.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/path_decoding.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer ses compétences en visualisation et lecture de plan, et apprendre à suivre des directions.




!!! info "Règle du jeu"

    Clique sur les cases pour déplacer Tux jusqu'à la cible en suivant la direction des flèches.<br><br>Les directions sont absolues. Elles ne dépendent pas de l'orientation actuelle de Tux.




___
![](../images/path_decoding.png){ width=50%;  : .center }

___
