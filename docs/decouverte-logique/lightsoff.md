# Éteins des lumières

## Éteins toutes les lumières

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/lightsoff.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer le sens de la stratégie, le sens de l'anticipation et le repérage dans l'espace.




!!! info "Règle du jeu"

    Le fait d'appuyer sur une lumière change l'état de cette lumière et de ses voisines verticales et horizontales immédiates. Tu dois éteindre toutes les lumières. Si tu cliques sur Tux, la solution est affichée.




___
![](../images/lightsoff.png){ width=50%;  : .center }

___
