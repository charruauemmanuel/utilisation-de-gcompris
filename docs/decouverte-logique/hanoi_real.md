# Tour de Hanoï

## Reproduis à l'identique la tour de gauche sur le piquet le plus à droite.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/hanoi_real.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer son sens de la stratégie.




!!! info "Règle du jeu"

    Déplace les disques afin que les disques se retrouvent autour du piquet le plus à droite par ordre de taille décroissante. Les disques peuvent aller et venir librement sur les piquets, en suivant deux règles : - on ne déplace qu’un seul disque à la fois ; - un disque ne peut jamais être posé sur un disque plus petit. Déplace un disque du dessus à la fois, d'une tour à l'autre, pour reproduire la tour de gauche sur le piquet de droite. Les disques peuvent aller et venir librement sur les piquets, en suivant deux règles : on ne déplace qu'un seul disque à la fois et un disque ne peut jamais être posé sur un disque plus petit que lui.




___
![](../images/hanoi_real.png){ width=50%;  : .center }

___
