# Mastermind

## Tux a caché une séquence de couleurs, retrouve-la dans le bon ordre.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/superbrain.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer sa logique, apprendre à distinguer des couleurs et mémoriser des positions.




!!! info "Règle du jeu"

    Appuie sur les éléments jusqu'à ce que tu penses avoir trouvé la bonne réponse. Ensuite, confirme en appuyant sur « Ok ». Dans les niveaux du début, Tux t'aide en entourant les bons éléments trouvés par un carré noir. Dans les niveaux 4 et 8 une couleur peut être utilisée pour différents éléments.<br/>Tu peux utiliser le bouton droit de ta souris pour choisir les couleurs dans le sens opposé ou utiliser le sélecteur de couleur pour choisir directement une couleur. Reste appuyé sur le bouton de la souris ou sur l'écran tactile pour automatiquement avoir la dernière couleur utilisée sur une case.




___
![](../images/superbrain.png){ width=50%;  : .center }

___
