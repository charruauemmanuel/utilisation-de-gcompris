# Tour de Hanoï simplifiée

## Reproduis la tour de droite sur le piquet vide.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/hanoi.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer son sens de la stratégie.




!!! info "Règle du jeu"

    Déplace un disque du dessus à la fois, d'une tour à l'autre, pour reproduire la tour de droite sur le piquet vide.




___
![](../images/hanoi.png){ width=50%;  : .center }

___
