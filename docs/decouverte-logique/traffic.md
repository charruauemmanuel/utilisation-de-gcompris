# Embouteillage

## Aide la voiture rouge à sortir de l'embouteillage en la guidant jusqu'à l'ouverture située à droite.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/decouverte-logique/traffic.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer l'anticipation et le repérage dans l'espace.




!!! info "Règle du jeu"

    Chaque voiture ne peut se déplacer que selon sa direction principale (horizontale ou verticale). Tu dois faire de la place en poussant les autres voitures afin que la rouge puisse sortir par l'ouverture à droite.




___
![](../images/traffic.png){ width=50%;  : .center }

___
