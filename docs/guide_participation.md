---
author: Emmanuel Charruau
title: 🏡 Comment contribuer au guide GCompris
---

# Guide participatif à l'utilisation de GCompris

La vidéo suivante explique pas à pas comment rajouter votre séance à la page de l'activité que vous souhaitez compléter.


[![Comment contribuer au guide GCompris](./images/screenshot_guide_gc.png)](https://tube-cycle-3.apps.education.fr/w/cEk6bH3fai5HKCdkhjHUFH "Comment contribuer au guide GCompris")

