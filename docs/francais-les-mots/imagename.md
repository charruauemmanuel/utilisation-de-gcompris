# Nomme l'image

## Attrape et fais glisser les éléments au-dessus de leurs noms.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-mots/imagename.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Enrichir son vocabulaire et pratiquer la lecture.




!!! info "Règle du jeu"

    Attrape et fais glisser les images situées dans le présentoir (à gauche) vers les emplacements qui correspondent à leur nom. Clique le bouton « Ok » pour vérifier ta réponse.




___
![](../images/imagename.png){ width=50%;  : .center }

___
