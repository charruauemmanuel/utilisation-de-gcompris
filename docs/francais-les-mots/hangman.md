# Le jeu du pendu

## Devine les lettres des mots présentés.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-mots/hangman.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Améliorer son orthographe et son vocabulaire.




!!! info "Règle du jeu"

    Pour écrire les lettres, tu peux soit utiliser le clavier virtuel affiché à l'écran ou utiliser ton clavier d'ordinateur.<br><br>Devine les lettres composant le mot donné. Pour t'aider, à chaque fois que tu tapes une lettre ne faisant pas partie du mot, une partie de l'image représentant le mot sera découverte.<br><br>Si l'option « Prononcer les mots à découvrir… » est activée, et si la voix correspondante est disponible, tu entendras le mot à découvrir lorsqu'il restera trois essais.




___
![](../images/hangman.png){ width=50%;  : .center }

___
