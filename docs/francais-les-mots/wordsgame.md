# Les mots qui tombent

## Saisis au clavier les mots qui tombent avant qu'ils ne touchent le sol.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-mots/wordsgame.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Pratiquer l'utilisation du clavier.




!!! info "Règle du jeu"

    Saisis au clavier les mots complets pendant qu'ils tombent et avant qu'ils ne touchent le sol.




___
![](../images/wordsgame.png){ width=50%;  : .center }

___
