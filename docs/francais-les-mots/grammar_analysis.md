# Analyse grammaticale

## Identifier les classes grammaticales dans les phrases.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-mots/grammar_analysis.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprends à identifier les classes grammaticales.




!!! info "Règle du jeu"

    Attribue les classes grammaticales demandées aux mots correspondants.<br>Sélectionne une classe grammaticale dans la liste, puis sélectionne la case sous un mot pour lui attribuer cette classe.<br>Laisse la case vide si aucune classe ne correspond.<br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches haut et bas ou tabulation : bascule entre les classes et les mots</li><li>Flèches gauche et droite : choisis l'élément dans les classes ou les mots</li><li>Espace : attribuer la classe sélectionnée au mot en cours et sélectionner le mot suivant</li><li>Touche retour arrière : choisir le mot précédent</li><li>Entrée : pour valider tes réponses</li></ul>




___
![](../images/grammar_analysis.png){ width=50%;  : .center }

___
