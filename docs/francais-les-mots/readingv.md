# Entraînement à la lecture verticale

## Lis la liste de mots à la verticale et détermine si un mot donné en fait partie.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-mots/readingv.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entraînement à la lecture en temps limité.




!!! info "Règle du jeu"

    Un mot est affiché sur le tableau. Une liste de mots affichés verticalement apparaîtra et disparaîtra. Est-ce que le mot appartient à cette liste ?




___
![](../images/readingv.png){ width=50%;  : .center }

___
