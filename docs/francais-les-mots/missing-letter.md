# Lettre manquante

## Trouve la lettre manquante pour compléter le mot.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-mots/missing-letter.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Savoir orthographier des mots.




!!! info "Règle du jeu"

    Un objet est affiché dans la zone principale. Un mot incomplet est inscrit sous l'image. Sélectionne la lettre manquante pour compléter le mot ou saisis cette lettre sur ton clavier.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Tab : répéter le mot</li></ul>




___
![](../images/missing-letter.png){ width=50%;  : .center }

___
