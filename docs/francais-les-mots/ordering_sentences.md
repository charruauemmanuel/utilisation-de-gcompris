# Ordonne les mots d'une phrase

## Mets les mots donnés dans le bon ordre pour former une phrase qui a du sens.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-mots/ordering_sentences.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à ordonner des mots pour former une phrase qui a du sens.




!!! info "Règle du jeu"

    Fais glisser les mots et dépose les dans la zone supérieure pour former une phrase qui a du sens.




___
![](../images/ordering_sentences.png){ width=50%;  : .center }

___
