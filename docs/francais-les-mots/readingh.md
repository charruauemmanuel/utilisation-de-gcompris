# Entraînement à la lecture horizontale

## Lis une liste de mots et détermine si un mot donné y figure.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-mots/readingh.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Entraînement à la lecture en temps limité.




!!! info "Règle du jeu"

    Un mot est affiché sur le tableau. Une liste de mots affichés horizontalement apparaîtra et disparaîtra. Est-ce que le mot appartient à cette liste ?




___
![](../images/readingh.png){ width=50%;  : .center }

___
