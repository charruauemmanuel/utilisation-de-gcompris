# Lettre dans quel mot

## Trouve les mots comprenant la lettre donnée.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/francais-les-mots/letter-in-word.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    S'entrainer à repérer une lettre donnée au milieu de mots.




!!! info "Règle du jeu"

    Une lettre est écrite et/ou parlée. Des mots sont affichés, l'enfant doit trouver le ou les mots contenant cette lettre.<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèches : se déplacer</li><li>Espace : sélectionner un élément</li><li>Entrée : pour valider tes réponses</li></ul>




___
![](../images/letter-in-word.png){ width=50%;  : .center }

___
