# Jeu de la barre (avec un ami)

## Anticipe et force l'autre joueur à poser une balle dans le dernier trou.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/strategie/bargame_2players.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer sa réflexion stratégique. Dénombrer, soustraire.




!!! info "Règle du jeu"

    Dans la zone verte, choisis le nombre de boules que tu vas placer en cliquant sur la boule. Attention, tu ne peux choisir qu'une des valeurs qui sont proposées. Quand la quantité choisie est affichée, clique sur « Ok ». Tes boules sont alors déposées dans les trous libres en partant de la gauche. Tu gagnes si ton ami pose la dernière boule. Tu dois mettre au point une stratégie qui te permettra d'éviter de poser la dernière boule (c'est-à-dire dans le trou marqué en rouge).




___
![](../images/bargame_2players.png){ width=50%;  : .center }

___
