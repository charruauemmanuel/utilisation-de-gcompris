# Joue aux échecs (contre Tux)

## Découvre et entraine-toi aux échecs.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/strategie/chess.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Apprendre à jouer aux échecs.




!!! info "Règle du jeu"

    Dans cette activité, tu découvres le jeu d'échecs en jouant contre l'ordinateur. Quand tu sélectionnes une pièce, tous ses mouvements possibles seront affichés afin de t'aider à comprendre comment les pièces se déplacent. Au premier niveau, l'ordinateur jouera de façon totalement aléatoire pour te donner plus de chance. Cependant, plus le niveau augmentera et mieux l'ordinateur jouera. Tu peux toi-même changer de niveau pour changer la difficulté.<br/><br/>Tu peux obtenir une victoire plus facilement si tu suis ces règles simples :<br/>Essaie d'amener le roi de l'adversaire dans le coin.<br/><b>Explication</b> : de cette façon, le roi adversaire n'aura plus que 3 directions pour se déplacer au lieu des 8 à partir d'une meilleure position.<br/>« Réalise un piège ». Utilise tes pions comme appât.<br/><b>Explication</b> : tu pourras attirer l'adversaire hors de sa zone de confort.<br/>Sois assez patient. <br/><b>Explication</b> : ne te précipite pas trop, sois patient. Pense un peu et essaye de prédire les futurs mouvements de ton adversaire, tu peux anticiper ses futurs coups et ainsi essayer de le piéger ou sécuriser tes pièces de ses attaques.<br><br>Un simple clic sur le bouton « Annuler » permettra d'annuler un déplacement. Un simple clic sur le bouton « Refaire » permet de refaire un déplacement. Pour annuler tous les mouvements, appuyez sur le bouton « Annuler » et maintenez-le enfoncé pendant 3 secondes.




___
![](../images/chess.png){ width=50%;  : .center }

___
