# Puissance 4 (avec un ami)

## Aligne quatre jetons.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/strategie/align4_2players.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer des compétences d'anticipation et de visualisation spatiale.




!!! info "Règle du jeu"

    Joue avec un ami. Tour à tour, clique sur la colonne sur laquelle tu veux déposer un jeton. Tu peux aussi utiliser les touches flèches pour déplacer le jeton à gauche ou à droite, et la touche flèche bas ou la barre espace pour faire tomber le jeton. Le premier qui aligne 4 jetons a gagné.<br>Crée un alignement de quatre jetons en les disposant horizontalement (couchés), verticalement (debout) ou encore en diagonale (en travers).<br><br><b>Pour contrôler avec le clavier :</b><ul><li>Flèche de gauche : déplacer le jeton à gauche</li><li>Flèche de droite : déplacer le jeton à droite</li><li>Espace ou flèche du bas : lâcher le jeton</li></ul>




___
![](../images/align4_2players.png){ width=50%;  : .center }

___
