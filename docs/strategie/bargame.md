# Jeu de la barre (contre Tux)

## Anticipe et force l'autre joueur à poser une balle dans le dernier trou.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/strategie/bargame.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer sa réflexion stratégique. Dénombrer, soustraire.




!!! info "Règle du jeu"

    Dans la zone verte, choisis le nombre de boules que tu vas placer en cliquant sur la boule. Attention, tu ne peux choisir qu'une des valeurs qui sont proposées. Quand la quantité choisie est affichée, clique sur « Ok ». Tes boules sont alors déposées dans les trous libres en partant de la gauche. Immédiatement après c'est Tux qui joue. Tu dois mettre au point une stratégie qui te permettra d'éviter de poser la dernière boule (c'est-à-dire dans le trou marqué en rouge).<br/>Si tu veux que ce soit Tux qui joue en premier, clique lui dessus dès le début.<br>Tu peux utiliser les flèches du clavier pour sélectionner manuellement le niveau de difficulté. Tux jouera mieux lorsque tu augmenteras le niveau.




___
![](../images/bargame.png){ width=50%;  : .center }

___
