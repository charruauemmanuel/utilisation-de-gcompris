# Joue aux dames (avec un ami)

## Capture toutes les pièces de ton adversaire avant qu'il ne capture les tiennes.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/strategie/checkers_2players.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    S'entrainer à jouer aux dames.




!!! info "Règle du jeu"

    Le jeu de dames se joue à 2 joueurs, chacun étant d'un côté du plateau. Un joueur a les pions noirs, l'autre les blancs. Le jeu se joue à tour de rôle. Un joueur ne peut pas déplacer un pion adverse. Un déplacement consiste à bouger un pion en diagonale vers une case inoccupée. Si la case contient un pion adverse et que celle juste après est vide, le pion doit être capturé (et retiré du jeu) en sautant par-dessus.<br/>Seules les cases noires du plateau sont utilisées. La capture est obligatoire (s'il y en a plusieurs possibles, celle qui capture le plus de pièces doit être réalisée). Le joueur qui n'a plus de pions ou qui ne peut plus bouger perd la partie.<br/>Quand un pion atteint la dernière ligne, il devient un roi et on rajoute un pion dessus pour le marquer en tant que tel. Il dispose de nouveaux pouvoirs dont la possibilité de se déplacer en arrière. S'il y a une pièce dans une diagonale que le roi peut capturer, il peut bouger à n'importe quelle distance dans cette diagonale et capturer ce pion quelle que soit la distance en sautant toutes les cases avant.<br/><br>Cette version du jeu de dames suit les règles du jeu de dames international.




___
![](../images/checkers_2players.png){ width=50%;  : .center }

___
