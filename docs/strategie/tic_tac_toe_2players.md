# Morpion (avec un ami)

## Place trois symboles sur la même ligne.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/strategie/tic_tac_toe_2players.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer ses compétences stratégiques.




!!! info "Règle du jeu"

    Joue avec un ami. Tour à tour, clique sur la case dans laquelle tu veux déposer un jeton. Tu peux aussi utiliser les touches flèches pour déplacer le jeton à gauche ou à droite, et la touche flèche bas ou la barre espace pour faire tomber le jeton. Le premier qui aligne trois jetons a gagné.




___
![](../images/tic_tac_toe_2players.png){ width=50%;  : .center }

___
