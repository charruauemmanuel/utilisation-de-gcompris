# Jeu du moulin (avec un ami)

## Joue au jeu du moulin avec un ami.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/strategie/nine_men_morris_2players.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer son sens de la stratégie et ses compétences de repérage dans l'espace.




!!! info "Règle du jeu"

    Joue avec un ami. Pour commencer, chaque joueur place à tour de rôle ses neuf pions. Ensuite, chaque joueur déplace à tour de rôle un de ses pions vers un emplacement libre adjacent, en essayant de créer un nouvel alignement de trois de ses jetons. S'il y arrive, il peut alors capturer un pion de son adversaire. Si un joueur n'a plus que trois pions, il peut les faire voler vers n'importe quel emplacement libre. Un joueur gagne lorsque l'autre joueur ne peut plus déplacer de pion ou s'il ne lui reste plus que deux pions.




___
![](../images/nine_men_morris_2players.png){ width=50%;  : .center }

___
