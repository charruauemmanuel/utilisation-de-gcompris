# Joue à l'Owaré (avec un ami)

## Fais une partie du jeu de stratégie Owaré avec un ami.

<span style="color: red;">Cette page vous a aidé à construire une séance à l'aide de GCompris ? Enrichissez la avec votre propre proposition d'utilisation de GCompris. Merci par avance de votre aide :)</span>

[Petit guide pour enrichir cette page](https://charruauemmanuel.forge.apps.education.fr/utilisation-de-gcompris/guide_participation/){ .md-button target="_blank" rel="noopener" }

[Cliquer ici pour éditer la page](https://forge.apps.education.fr/-/ide/project/charruauemmanuel/utilisation-de-gcompris/edit/main/-/docs/strategie/oware_2players.md){ .md-button target="_blank" rel="noopener" }

!!! warning "Objectif pédagogique"

    Développer sa réflexion stratégique.




!!! info "Règle du jeu"

    Le jeu commence avec quatre graines dans chaque maison. Le but du jeu est de capturer plus de graines que son adversaire. Comme le jeu ne comporte que 48 graines, il suffit d'en capturer 25 pour gagner la partie. Comme il y a un nombre pair de graines, il est possible que le jeu se termine par un match nul, où chaque joueur a capturé 24 graines.<br/><br/>Les joueurs déplacent les graines à tour de rôle. Lors de son tour, un joueur choisit l'une des six maisons qu'il contrôle. Le joueur retire toutes les graines de cette maison et les distribue, en en déposant une dans chaque maison dans le sens inverse des aiguilles d'une montre à partir de cette maison, dans un processus appelé semis. Les graines ne sont pas distribuées ni dans les maisons marquant le point final, ni dans la maison de départ. La maison de départ est toujours laissée vide. Si elle contient 12 graines (ou plus), elle est ignorée et la douzième graine est placée dans la maison suivante.<br/><br/>Une fois que toutes les graines ont été semées, la capture se produit seulement si la dernière maison ayant reçu des graines contient exactement deux ou trois graines. Si l'avant-dernière graine a également porté le nombre de graines de la maison de l'adversaire à deux ou trois, celles-ci sont également capturées, et ainsi de suite jusqu'à ce qu'une maison ne contenant pas deux ou trois graines ou n'appartenant pas à l'adversaire soit atteinte. Les graines capturées sont placées dans la maison du joueur marquant les points. Cependant, si un coup permet de capturer toutes les graines de l'adversaire, la capture est abandonnée car elle empêcherait l'adversaire de continuer la partie et les graines sont laissées sur le plateau.<br/><br/>Si les maisons de l'adversaire sont toutes vides, le joueur doit effectuer un mouvement donnant des graines à l'adversaire. Si un tel mouvement n'est pas possible, il capture toutes les graines sur son propre territoire, mettant ainsi fin à la partie.




___
![](../images/oware_2players.png){ width=50%;  : .center }

___
